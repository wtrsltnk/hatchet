# Hatchet

A csg level builder like Valve Hammer Editor. Should be able to build .map files.

![image](screenshot05.png "Screenshot 5, shows the new grid")

![image](screenshot04.png "Screenshot 4, shows the new flat rendering perspective")

![image](screenshot03.png "Screenshot 3, shows the texture browser with filtered textures from multiple loaded wad files")

![image](screenshot02.png "Screenshot 2, shows the texture browser")

![image](screenshot01.png "Screenshot 1")
