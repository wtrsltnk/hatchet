#ifndef VIEWNAMES_H
#define VIEWNAMES_H

enum class ViewNames
{
    BottomLeft = 0,
    TopLeft = 1,
    BottomRight = 2,
    TopRight = 3,
    Unknown = 4,
};

#endif // VIEWNAMES_H
