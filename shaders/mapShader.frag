#version 150

uniform sampler2D u_tex;
uniform int u_view_type;
uniform vec4 u_brush_color;

in vec2 f_uv;
in vec4 f_color;
out vec4 fragColor;

void main()
{

//    Top,
    if (u_view_type == 0)
    {
        fragColor = f_color * u_brush_color;
    }
//    Front,
    if (u_view_type == 1)
    {
        fragColor = f_color * u_brush_color;
    }
//    Side,
    if (u_view_type == 2)
    {
        fragColor = f_color * u_brush_color;
    }
//    PerspectiveWireframe,
    if (u_view_type == 3)
    {
        fragColor = f_color * u_brush_color;
    }
//    PerspectiveFlat,
    if (u_view_type == 4)
    {
        fragColor = f_color * u_brush_color;
    }
//    PerspectiveTextured
    if (u_view_type == 5)
    {
       fragColor = f_color * texture(u_tex, f_uv.st);
    }
//    Grid
    if (u_view_type == 6)
    {
        if (f_uv.s > 0.5) fragColor = u_brush_color;
        else fragColor = vec4(u_brush_color.rgb, 0.4);
    }
}
