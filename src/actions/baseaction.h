#ifndef BASEACTION_H
#define BASEACTION_H

#include <glad/glad.h>

class Image;

class BaseAction
{
public:
    BaseAction();
};

class BaseActionFactory
{
public:
    void OnActivateTool(
        struct app_state &state);

    virtual void MouseMove(
        int x,
        int y) = 0;

    virtual void PrimaryMouseButtonDown(
        int x,
        int y,
        bool shift,
        bool ctrl,
        bool alt,
        bool super) = 0;

    virtual void PrimaryMouseButtonUp(
        int x,
        int y,
        bool shift,
        bool ctrl,
        bool alt,
        bool super) = 0;

    virtual void SecondaryMouseButtonDown(
        int x,
        int y,
        bool shift,
        bool ctrl,
        bool alt,
        bool super) = 0;

    virtual void SecondaryMouseButtonUp(
        int x,
        int y,
        bool shift,
        bool ctrl,
        bool alt,
        bool super) = 0;

    virtual void RenderTool() = 0;

    virtual GLuint ToolHelperImage() = 0;

protected:
    BaseActionFactory();

    struct app_state *_state;
};

#endif // BASEACTION_H
