#include "selectionaction.h"

#include "program.h"

SelectionActionFactory::SelectionActionFactory() = default;

void SelectionActionFactory::MouseMove(
    int x,
    int y)
{
    (void)x;
    (void)y;

    selectionIsValid = true;
}

void SelectionActionFactory::PrimaryMouseButtonDown(
    int x,
    int y,
    bool shift,
    bool ctrl,
    bool alt,
    bool super)
{
    (void)shift;
    (void)ctrl;
    (void)alt;
    (void)super;

    // selectingView = _state->focusedView;
    selectingStartedAt = glm::vec2(x, y);
    selectionIsValid = true;
}

void SelectionActionFactory::PrimaryMouseButtonUp(
    int x,
    int y,
    bool shift,
    bool ctrl,
    bool alt,
    bool super)
{
    (void)x;
    (void)y;
    (void)shift;
    (void)alt;
    (void)super;

    if (_state == nullptr)
    {
        return;
    }

    auto startAt = ImGuiGlobalToViewLocal(ImVec2(selectingStartedAt.x, selectingStartedAt.y), _state->views[_state->focusedView], *_state);
    auto endAt = ImGuiGlobalToViewLocal(ImGui::GetMousePos(), _state->views[_state->focusedView], *_state);

    if (!ctrl)
    {
        _state->selectedBrushes.clear();
        _state->selectedEntities.clear();
    }

    auto selection = _state->selectionService.Select(
        _state->scene,
        _state->views[_state->focusedView]->ViewMatrix(),
        _state->views[_state->focusedView]->ProjectionMatrix(),
        glm::vec4(0.0f, 0.0f, float(_state->width), float(_state->height)),
        startAt,
        endAt);

    _state->selectedBrushes.insert(selection.selectedBrushes.begin(), selection.selectedBrushes.end());
    _state->selectedEntities.insert(selection.selectedEntities.begin(), selection.selectedEntities.end());

    selectionIsValid = false;
}

void SelectionActionFactory::SecondaryMouseButtonDown(
    int x,
    int y,
    bool shift,
    bool ctrl,
    bool alt,
    bool super)
{
    (void)x;
    (void)y;
    (void)shift;
    (void)ctrl;
    (void)alt;
    (void)super;
}

void SelectionActionFactory::SecondaryMouseButtonUp(
    int x,
    int y,
    bool shift,
    bool ctrl,
    bool alt,
    bool super)
{
    (void)x;
    (void)y;
    (void)shift;
    (void)ctrl;
    (void)alt;
    (void)super;
}

void SelectionActionFactory::RenderTool()
{
    auto startAt = ImVec2(selectingStartedAt.x, selectingStartedAt.y);

    if (selectionIsValid)
    {
        ImGui::GetOverlayDrawList()->AddRectFilled(
            startAt,
            ImGui::GetMousePos(),
            ImGui::ColorConvertFloat4ToU32(ImVec4(1.0f, 0.0f, 0.0f, 0.4f)));

        ImGui::GetOverlayDrawList()->AddRect(
            startAt,
            ImGui::GetMousePos(),
            ImGui::ColorConvertFloat4ToU32(ImVec4(1.0f, 1.0f, 0.0f, 0.7f)));
    }
}

GLuint SelectionActionFactory::ToolHelperImage()
{
    return 0;
}
