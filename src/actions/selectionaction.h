#ifndef SELECTIONACTION_H
#define SELECTIONACTION_H

#include "baseaction.h"
#include <glad/glad.h>
#include <glm/glm.hpp>

class SelectionActionFactory : public BaseActionFactory
{
public:
    SelectionActionFactory();

    virtual void MouseMove(
        int x,
        int y);

    virtual void PrimaryMouseButtonDown(
        int x,
        int y,
        bool shift,
        bool ctrl,
        bool alt,
        bool super);

    virtual void PrimaryMouseButtonUp(
        int x,
        int y,
        bool shift,
        bool ctrl,
        bool alt,
        bool super);

    virtual void SecondaryMouseButtonDown(
        int x,
        int y,
        bool shift,
        bool ctrl,
        bool alt,
        bool super);

    virtual void SecondaryMouseButtonUp(
        int x,
        int y,
        bool shift,
        bool ctrl,
        bool alt,
        bool super);

    virtual void RenderTool();

    virtual GLuint ToolHelperImage();

private:
    glm::vec2 selectingStartedAt;
    bool selectionIsValid = false;
};

#endif // SELECTIONACTION_H
