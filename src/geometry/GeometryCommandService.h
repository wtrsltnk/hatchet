#ifndef GEOMETRYCOMMANDSERVICE_H
#define GEOMETRYCOMMANDSERVICE_H

#include "mapscene.h"
#include <string>

class LoadSceneModel
{
public:
    virtual ~LoadSceneModel() {}

    std::string Data;
};

class GeometryCommandService
{
public:
    GeometryCommandService(
        MapScene *scene);

    virtual ~GeometryCommandService();

    void LoadScene(
        std::string const &data);
};

#endif // GEOMETRYCOMMANDSERVICE_H
