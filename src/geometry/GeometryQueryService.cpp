#include "GeometryQueryService.h"
#include "brushops.h"
#include "mapbrush.h"
#include "mapbrushside.h"
#include "mapentity.h"
#include "mapscene.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>
#include <iostream>

MapVertex::MapVertex(
    glm::vec3 const &pos,
    glm::vec3 const &normal,
    glm::vec3 const &uv)
    : _pos(pos),
      _normal(normal),
      _uv(uv)
{}

MapFace::MapFace(
    const std::string &texture,
    int first,
    int count)
    : _texture(texture),
      _firstVertex(first),
      _vertexCount(count)
{}

GeometryQueryService::GeometryQueryService()
{}

void GeometryQueryService::Clear()
{
    this->_data.clear();
    this->_verts.clear();
}

void GeometryQueryService::Collect(
    const MapScene &scene)
{
    this->Collect(scene._entities);
}

void GeometryQueryService::Collect(
    const std::vector<MapEntity> &entities)
{
    for (const auto &entity : entities)
    {
        this->Collect(entity);
    }
}

void GeometryQueryService::Collect(
    const MapEntity &entity)
{
    for (const MapBrush &brush : entity._brushes)
    {
        winding_t **windings = new winding_t *[brush._sides.size()];

        for (int i = 0; i < int(brush._sides.size()); i++)
        {
            auto side = brush._sides[i];
            auto planei = Plane(side._distance, side._normal);
            windings[i] = CreateWindingFromPlane(&planei);
            for (int j = 0; j < int(brush._sides.size()); j++)
            {
                if (i != j && windings[i] != 0)
                {
                    auto planej = new Plane(brush._sides[j]._distance, brush._sides[j]._normal);
                    windings[i] = ClipWinding(windings[i], planej);
                }
            }
            if (windings[i] != 0)
            {
                MapFace face(side._material._texture, this->_verts.size(), windings[i]->numpoints);
                face._color = brush._color;
                face._brushId = brush.id();
                face._brushSideId = side.id();

                // Change order of vertices, otherwise it will show 'backsides'
                for (int k = 0; k < windings[i]->numpoints; k++)
                {
                    glm::vec3 a = windings[i]->p[k];
                    this->_verts.push_back(MapVertex(a, side._normal, glm::vec3()));
                }
                this->UpdateTextureData(side, face);
                auto found = this->_data.find(side._material._texture);
                if (found == this->_data.end())
                {
                    this->_data.insert(this->_data.end(), std::make_pair(side._material._texture, std::vector<MapFace>()));
                }
                this->_data[side._material._texture].push_back(face);
            }
        }

        // To prevent memory leak, cleanup the used windings
        for (int i = 0; i < int(brush._sides.size()); i++)
        {
            if (windings[i] != 0)
            {
                FreeWinding(windings[i]);
            }
        }

        delete[] windings;
    }
}

void GeometryQueryService::UpdateTextureData(
    MapBrushSide &side,
    const MapFace &face)
{
    // TODO : get the texture width/height from the texture/material manager
    auto textureWidth = 32;
    auto textureHeight = 32;

    float W, H, SX = 1.0f, SY = 1.0f;
    W = 1.0f / float(textureWidth);
    H = 1.0f / float(textureHeight);
    SX = 1.0f / float(side._material._scalex);
    SY = 1.0f / float(side._material._scaley);

    for (int i = 0; i < face._vertexCount; i++)
    {
        glm::vec3 v = this->_verts[face._firstVertex + i]._pos;
        this->_verts[i]._uv.x = (glm::dot(v, side._material._axis1) * W * SX) + (side._material._offset1 * W);
        this->_verts[i]._uv.y = (glm::dot(v, side._material._axis2) * H * SY) + (side._material._offset2 * H);
    }
}
