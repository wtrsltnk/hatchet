#ifndef GEOMETRYQUERYSERVICE_H
#define GEOMETRYQUERYSERVICE_H

#include "mapentity.h"
#include "mapscene.h"

#include <glm/glm.hpp>
#include <map>
#include <string>
#include <vector>

struct MapVertex
{
    MapVertex(
        glm::vec3 const &pos,
        glm::vec3 const &normal,
        glm::vec3 const &uv);

    glm::vec3 _pos;
    glm::vec3 _normal;
    glm::vec3 _uv;
};

class MapFace
{
public:
    MapFace(
        const std::string &texture,
        int first,
        int count);

    long _brushId;
    long _brushSideId;
    std::string _texture;
    int _firstVertex;
    int _vertexCount;
    glm::vec4 _color;
};

class GeometryQueryService
{
public:
    GeometryQueryService();

    void Clear();

    void Collect(
        const MapScene &scene);

    void Collect(
        const std::vector<MapEntity> &entities);

    void Collect(
        const MapEntity &entity);

    void UpdateTextureData(
        class MapBrushSide &side,
        const class MapFace &face);

    std::map<std::string, std::vector<MapFace>> _data;
    std::vector<MapVertex> _verts;
};

#endif // GEOMETRYQUERYSERVICE_H
