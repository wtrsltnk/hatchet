#include "mapbrush.h"
#include "mapbrushside.h"

static long MapBrushIdCounter = 1;

static unsigned long randomSeed = 0;

unsigned long nextNumbr()
{
    randomSeed = randomSeed * 214013L + 20171010L;

    return randomSeed;
}

MapBrush::MapBrush()
    : _id(MapBrushIdCounter++)
{
    _color = glm::vec4(
        0,
        (100 + (nextNumbr() % 156)) / 255.0f,
        (100 + (nextNumbr() % 156)) / 255.0f,
        1.0f);
}

MapBrush::MapBrush(
    const MapBrush &cloneFrom)
    : _id(cloneFrom._id)
{
    (*this) = cloneFrom;
}

MapBrush &MapBrush::operator=(
    const MapBrush &cloneFrom)
{
    _id = cloneFrom._id;
    _color = cloneFrom._color;
    _sides.clear();

    for (const auto &side : cloneFrom._sides)
    {
        _sides.push_back(MapBrushSide(side));
    }

    return (*this);
}
