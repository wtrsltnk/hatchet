#ifndef MAPBRUSH_H
#define MAPBRUSH_H

#include "mapbrushside.h"
#include <glm/glm.hpp>
#include <vector>

class MapBrush
{
public:
    MapBrush();

    MapBrush(
        const MapBrush &cloneFrom);

    MapBrush &operator=(
        const MapBrush &cloneFrom);

    std::vector<MapBrushSide> _sides;
    glm::vec4 _color;

    long id() const { return _id; }

private:
    long _id;
};

#endif // MAPBRUSH_H
