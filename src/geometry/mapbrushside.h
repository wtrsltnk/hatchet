#ifndef MAPBRUSHSIDE_H
#define MAPBRUSHSIDE_H

#include "mapbrushsidematerial.h"
#include <glm/glm.hpp>

class MapBrushSide
{
public:
    MapBrushSide();

    MapBrushSide(
        const MapBrushSide &cloneFrom);

    MapBrushSide &operator=(
        const MapBrushSide &cloneFrom);

    glm::vec3 _normal;
    float _distance;
    MapBrushSideMaterial _material;

    long id() const { return _id; }

public:
    static MapBrushSide FromVertices(
        const glm::vec3 a,
        const glm::vec3 &b,
        const glm::vec3 &c);

private:
    long _id;
};

#endif // MAPBRUSHSIDE_H
