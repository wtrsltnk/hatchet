#include "mapentity.h"

static long MapEntityIdCounter = 1;

MapEntity::MapEntity()
    : _id(MapEntityIdCounter++)
{}

MapEntity::MapEntity(
    const MapEntity &cloneFrom)
    : _id(cloneFrom._id)
{
    (*this) = cloneFrom;
}

MapEntity &MapEntity::operator=(
    const MapEntity &cloneFrom)
{
    _id = cloneFrom._id;

    for (const auto &brush : cloneFrom._brushes)
    {
        _brushes.push_back(MapBrush(brush));
    }

    for (const auto &kvp : cloneFrom._keyValuePairs)
    {
        _keyValuePairs.push_back(MapKeyValuePair(kvp._key, kvp._value));
    }

    return (*this);
}
