#ifndef MAPENTITY_H
#define MAPENTITY_H

#include "mapbrush.h"
#include "mapkeyvaluepair.h"
#include <vector>

class MapEntity
{
public:
    MapEntity();

    MapEntity(
        const MapEntity &cloneFrom);

    MapEntity &operator=(
        const MapEntity &cloneFrom);

    std::vector<MapBrush> _brushes;
    std::vector<MapKeyValuePair> _keyValuePairs;

    long id() const { return _id; }

private:
    long _id;
};

#endif // MAPENTITY_H
