#ifndef MAPOPERATIONS_H
#define MAPOPERATIONS_H

#include <glm/glm.hpp>
#include <vector>

class MapOperations
{
public:
    MapOperations();

    void Clip(
        const std::vector<class MapBrush *> &brushes,
        const glm::vec4 &plane);
};

#endif // MAPOPERATIONS_H
