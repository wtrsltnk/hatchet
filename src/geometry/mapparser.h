#ifndef MAPPARSER_H
#define MAPPARSER_H

#include <functional>
#include <string>

#include "mapbrush.h"
#include "mapentity.h"
#include "mapscene.h"

enum class LogLevels
{
    Fatal,
    Error,
    Warning,
    Info,
    Debug,
    Trace
};

class MapParser
{
    bool returnFalseAndLog(const std::string &message);

    class Tokenizer *_tok;
    std::function<void(LogLevels, const std::string &)> _logger;

public:
    MapParser(const std::string &content);

    bool LoadScene(
        MapScene &scene);

    bool LoadEntity(
        MapEntity &entity);

    bool LoadBrush(
        MapBrush &brush);

    void SetLogger(std::function<void(LogLevels, const std::string &)> logger);
};

#endif // MAPPARSER_H
