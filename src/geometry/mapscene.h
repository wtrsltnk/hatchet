#ifndef MAPSCENE_H
#define MAPSCENE_H

#include "mapentity.h"
#include <vector>

enum SceneFlags
{
    Modified = 1 << 1,
};

class MapScene
{
public:
    MapScene();

    std::vector<MapEntity> _entities;
    int _flags;

    long id() const { return _id; }

private:
    long _id;
};

#endif // MAPSCENE_H
