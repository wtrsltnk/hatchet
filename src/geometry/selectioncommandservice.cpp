#include "selectioncommandservice.h"

#include <random>

SelectionCommandService::SelectionCommandService()
{
}

Selection SelectionCommandService::Select(
    const MapScene &scene,
    const glm::mat4 &model,
    const glm::mat4 &projection,
    const glm::vec4 &viewport,
    const glm::vec2 &selectionRectCorner1,
    const glm::vec2 &selectionRectCorner2)
{
    glm::vec2 min = glm::vec2(
        glm::min(selectionRectCorner1.x, selectionRectCorner2.x),
        glm::min(selectionRectCorner1.y, selectionRectCorner2.y));

    glm::vec2 max = glm::vec2(
        glm::max(selectionRectCorner1.x, selectionRectCorner2.x),
        glm::max(selectionRectCorner1.y, selectionRectCorner2.y));

    Selection result;

    for (auto i = 0; i < 20; i++)
    {
        result.selectedBrushes.insert(std::rand() % 100);
    }

    return result;
}
