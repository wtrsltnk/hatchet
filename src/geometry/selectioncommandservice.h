#ifndef SELECTIONCOMMANDSERVICE_H
#define SELECTIONCOMMANDSERVICE_H

#include "mapscene.h"

#include <glm/glm.hpp>
#include <set>

struct Selection
{
    std::set<long> selectedBrushes;
    std::set<long> selectedEntities;
};

class SelectionCommandService
{
public:
    SelectionCommandService();

    Selection Select(
        const MapScene &scene,
        const glm::mat4 &model,
        const glm::mat4 &projection,
        const glm::vec4 &viewport,
        const glm::vec2 &selectionRectCorner1,
        const glm::vec2 &selectionRectCorner2);
};

#endif // SELECTIONCOMMANDSERVICE_H
