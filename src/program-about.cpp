#include "program.h"
#include <imgui.h>

void Program::renderGuiAbout()
{
    if (ImGui::BeginPopupModal("About", NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings))
    {
        state.modalActive = true;
        if (ImGui::Button("OK", ImVec2(120, 0)))
        {
            state.modalActive = false;
            ImGui::CloseCurrentPopup();
        }
        ImGui::EndPopup();
    }
}
