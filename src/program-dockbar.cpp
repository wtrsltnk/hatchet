#include "program.h"
#include <algorithm>
#include <imgui.h>
#include <sstream>

void Program::renderGuiDockbar()
{
    const float previewSize = 150.0f;
    ImGui::Begin("dockbar", &(state.show_dockbar), ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar);
    {
        ImGui::SetWindowPos(ImVec2(state.width - dockbarWidth, float(menuBarHeight)));
        ImGui::SetWindowSize(ImVec2(float(dockbarWidth), state.height - menuBarHeight - statusBarHeight));

        if (ImGui::CollapsingHeader("Texture options", "colors", true, true))
        {
            if (state.currentTexture != nullptr)
            {
                auto aspect = (std::min)(previewSize / state.currentTexture->width(), previewSize / state.currentTexture->height());

                ImGui::Text("%s", state.currentTexture->name().c_str());
                ImGui::BeginChild("current texture", ImVec2(previewSize, previewSize));
                ImGui::Image(reinterpret_cast<ImTextureID>(state.currentTexture->glId()), ImVec2(state.currentTexture->width() * aspect, state.currentTexture->height() * aspect));
                ImGui::EndChild();
            }
            else
            {
                ImGui::Text("No current texture selected");
                ImGui::Dummy(ImVec2(previewSize, previewSize));
            }

            if (ImGui::IsItemHovered())
            {
                ImGui::SetTooltip("Current selected texture");
            }

            ImGui::SameLine();
            ImGui::BeginGroup();
            if (state.currentTexture != nullptr)
            {
                std::stringstream ss;
                ss << state.currentTexture->width() << "x" << state.currentTexture->height();
                ImGui::Text("%s", ss.str().c_str());
            }
            if (ImGui::Button("Browse..."))
            {
                ImGui::OpenPopup("Browse texture");
            }
            ImGui::Button("Replace...");
            ImGui::EndGroup();

            auto result = renderGuiTextureBrowser();
            if (result != nullptr)
            {
                state.currentTexture = result;
            }
        }

        if (ImGui::CollapsingHeader("Visgroups options", "tools", true, true))
        {
            static int selectedVisGroup = 0;
            static const char *const values[] = {"string one", "string two"};
            ImGui::ListBox("", &selectedVisGroup, values, 2, 4);
            ImGui::Button("Apply");
            ImGui::SameLine();
            ImGui::Button("Edit");
            ImGui::SameLine();
            ImGui::Button("Mark");
            ImGui::SameLine();
            ImGui::Button("Purge");
        }

        if (ImGui::CollapsingHeader("Entity options", "tools", true, true))
        {
            ImGui::Text("Move selected:");
            ImGui::SameLine();
            ImGui::Button("to World");
            ImGui::SameLine();
            ImGui::Button("to Entity");
            static int selectedCategory = 0;
            const char *items = "test 1\0test 2\0test 3\0\0";
            ImGui::Combo("Categories", &selectedCategory, items, 4);
            static int selectedObject = 0;
            const char *objects = "test 1\0test 2\0test 3\0\0";
            ImGui::Combo("Objects", &selectedObject, objects, 4);

            ImGui::Separator();
            static int faces = 0;
            ImGui::InputInt("Faces", &faces, 1, 100);
            ImGui::Button("Create Prefab");
        }
    }
    ImGui::End();
}
