#include "program.h"
#include <fstream>
#include <imgui.h>
#include <noc_file_dialog.h>
#include <streambuf>

#include <geometry/mapparser.h>

void Program::renderGuiMenu()
{
    bool runAbout = false;
    bool runOptions = false;

    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4());
    {
        if (ImGui::BeginMainMenuBar())
        {
            if (ImGui::BeginMenu("File"))
            {
                if (ImGui::MenuItem("New", "CTRL+N")) newMap();
                if (ImGui::MenuItem("Open", "CTRL+O")) openMap();
                if (ImGui::MenuItem("Save", "CTRL+S")) saveMap();
                if (ImGui::MenuItem("Save As..", "CTRL+SHIFT+Z")) saveMapAs();
                if (ImGui::MenuItem("Close")) closeMap();
                ImGui::Separator();
                if (ImGui::MenuItem("Export")) exportMap();
                if (ImGui::MenuItem("Export Again")) exportMapAgain();
                ImGui::Separator();
                if (ImGui::MenuItem("Quit")) quitApp();
                ImGui::EndMenu(); // File
            }
            if (ImGui::BeginMenu("Edit"))
            {
                if (ImGui::MenuItem("Undo", "CTRL+Z")) undo();
                if (ImGui::MenuItem("Redo", "CTRL+Y", false, false)) redo();
                ImGui::Separator();
                if (ImGui::MenuItem("Cut", "CTRL+X")) cut();
                if (ImGui::MenuItem("Copy", "CTRL+C")) copy();
                if (ImGui::MenuItem("Paste", "CTRL+V")) paste();
                ImGui::EndMenu(); // Edit
            }
            if (ImGui::BeginMenu("Tools"))
            {
                if (ImGui::MenuItem("Options...")) runOptions = true;
                ImGui::EndMenu(); // Tools
            }
            if (ImGui::BeginMenu("Help"))
            {
                if (ImGui::MenuItem("About Hatchet")) runAbout = true;
                ImGui::EndMenu(); // Help
            }
            ImGui::EndMainMenuBar();
        }
    }
    ImGui::PopStyleColor();

    if (runAbout)
    {
        ImGui::OpenPopup("About");
    }
    renderGuiAbout();

    static app_options tempOpts;
    if (runOptions)
    {
        ImGui::OpenPopup("Options");
        tempOpts = options;
    }
    if (renderGuiOptions(tempOpts))
    {
        populateTextureManagerFromOptions();
    }
}

void Program::loadMap(std::string const &filename)
{
    std::ifstream t(filename);
    std::string str((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

    MapParser parser(str);

    parser.LoadScene(state.scene);

    state.renderData.Collect(state.scene);

    state.vertexBuffer.UploadVertexData(state.renderData._verts);
}

void Program::newMap()
{
    closeMap();
}

void Program::openMap()
{
    const char *file = noc_file_dialog_open(NOC_FILE_DIALOG_OPEN, "Map file (*.map)\0*.map", nullptr, nullptr);

    if (file != nullptr)
    {
        loadMap(file);
    }
}

void Program::saveMap() {}

void Program::saveMapAs() {}

void Program::exportMap() {}

void Program::exportMapAgain() {}

void Program::closeMap()
{
    state.renderData.Clear();
}

void Program::quitApp()
{
    closeMap();
    glfwSetWindowShouldClose(_window, GLFW_TRUE);
}

void Program::undo() {}

void Program::redo() {}

void Program::cut() {}

void Program::copy() {}

void Program::paste() {}

void Program::about() {}
