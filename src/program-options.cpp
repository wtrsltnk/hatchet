#include "imgui_tabs.h"
#include "program.h"
#include <filesystem>
#include <imgui.h>
#include <noc_file_dialog.h>
#include <vector>

bool Program::renderGuiOptions(app_options &opt)
{
    bool result = false;

    ImGui::SetNextWindowContentSize(ImVec2(400, 400));
    if (ImGui::BeginPopupModal("Options", NULL, ImGuiWindowFlags_AlwaysAutoResize))
    {
        state.modalActive = true;

        static const char *tabNames[] = {"Path options", "Key mappings"};
        static int tabOrder[] = {0, 1};
        static int tabSelected = 0;
        const bool tabChanged = ImGui::TabLabels(tabNames, sizeof(tabNames) / sizeof(tabNames[0]), tabSelected, tabOrder);

        if (tabSelected == 0)
        {
            char hlExecutablePath[MAX_PATH];
            strcpy_s(hlExecutablePath, MAX_PATH, opt.hlExecutablePath.c_str());
            ImGui::Text("Full path to half-life executable");
            if (ImGui::InputText("##HalfLifePath", hlExecutablePath, IM_ARRAYSIZE(hlExecutablePath), ImGuiInputTextFlags_ReadOnly))
            {
                opt.hlExecutablePath = hlExecutablePath;
            }
            ImGui::SameLine();
            if (ImGui::Button("Find hl.exe"))
            {
                const char *file = noc_file_dialog_open(NOC_FILE_DIALOG_OPEN, "Half-Life executable (hl.exe)\0hl.exe", nullptr, nullptr);
                if (file != nullptr)
                {
                    opt.hlExecutablePath = file;
                }
            }

            char mod[MAX_PATH];
            strcpy_s(mod, MAX_PATH, opt.mod.c_str());
            ImGui::Text("Modification");
            if (ImGui::InputText("##Modification", mod, IM_ARRAYSIZE(mod), ImGuiInputTextFlags_CharsNoBlank))
            {
                opt.mod = mod;
            }
        }
        if (tabSelected == 1)
        {
            ImGui::Columns(2);
            ImGui::SetColumnWidth(0, 220);

            ImGui::Text("Action");
            ImGui::NextColumn();
            ImGui::Text("Bound keys");
            ImGui::NextColumn();

            ImGui::Separator();

            for (int i = 0; i < int(UserInputActions::Count); ++i)
            {
                if (ImGui::Button(UserInputActionNames[i], ImVec2(200, 36)))
                {
                    _userInput.StartMappingAction((UserInputActions)i);
                }
                ImGui::NextColumn();

                auto mappedEvents = _userInput.GetMappedActionEvents((UserInputActions)i);
                bool first = true;
                for (auto e : mappedEvents)
                {
                    if (!first)
                    {
                        ImGui::Text("or");
                        ImGui::SameLine();
                    }
                    ImGui::Text("%s", e.toString());
                    ImGui::SameLine();
                    first = false;
                }
                if (mappedEvents.size() == 0)
                {
                    ImGui::Text("<unbound>");
                }
                ImGui::NextColumn();
            }
            ImGui::Columns(1);
        }

        ImGui::Separator();

        if (ImGui::Button("Save", ImVec2(120, 0)))
        {
            auto kepmapFile = std::filesystem::path(_settingsDir) / KEYMAP_FILE;
            _userInput.WriteKeyMappings(kepmapFile.string());

            state.modalActive = false;
            ImGui::CloseCurrentPopup();
            options = opt;
            result = true;
        }
        ImGui::SameLine();
        if (ImGui::Button("Cancel", ImVec2(120, 0)))
        {
            state.modalActive = false;
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    return result;
}
