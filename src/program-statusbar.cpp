#include "program.h"
#include <imgui.h>

void Program::renderGuiStatusbar()
{
    ImGui::Begin("statusbar", &(state.show_content), ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar);
    {
        ImGui::SetWindowPos(ImVec2(0, state.height - statusBarHeight));
        ImGui::SetWindowSize(ImVec2(state.width, float(statusBarHeight)));

        ImGui::Columns(3);
        ImGui::Text("status bar");
        ImGui::NextColumn();
        ImGui::Text("focused view is :%d", (int)state.focusedView);
        ImGui::Columns(1);
    }
    ImGui::End();
}
