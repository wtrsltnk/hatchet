#include "program.h"
#include "texturemanager.h"
#include <algorithm>
#include <imgui.h>
#include <sstream>

Texture *Program::renderGuiTextureBrowser()
{
    Texture *result = nullptr;
    static char search_for[45];

    state.show_texture_browser = false;
    ImGui::SetNextWindowContentSize(ImVec2(800, 510));
    if (ImGui::BeginPopupModal("Browse texture", NULL, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar))
    {
        state.modalActive = true;
        state.show_texture_browser = true;

        ImGui::BeginChild("filter_textures", ImVec2(ImGui::GetWindowContentRegionWidth(), 30));
        ImGui::Text("Filter");
        ImGui::SameLine();
        if (ImGui::InputText("##search_for", search_for, IM_ARRAYSIZE(search_for), ImGuiInputTextFlags_CharsUppercase))
        {
            state.foundTextures = textures.findTextures(search_for);
        }
        ImGui::EndChild();

        ImGui::BeginChild("stextures", ImVec2(ImGui::GetWindowContentRegionWidth(), 435));
        for (auto pair : state.foundTextures)
        {
            if (ImGui::CollapsingHeader(pair.first.c_str()))
            {
                int i = 0;
                for (auto texture : pair.second)
                {
                    if (texture->glId() == 0) texture->upload();

                    auto aspect = (std::min)(110.0f / texture->width(), 110.0f / texture->height());

                    if (i++ % 6 != 0) ImGui::SameLine();

                    std::stringstream ss;
                    ss << "textureimage" << i;
                    ImGui::BeginGroup();
                    ImGui::PushStyleColor(ImGuiCol_Button, (texture == state.currentTexture ? ImVec4(1, 0, 0, 1) : ImVec4(0, 0, 0, 0)));
                    if (ImGui::ImageButton((ImTextureID)texture->glId(), ImVec2(texture->width() * aspect, texture->height() * aspect)))
                    {
                        result = texture;
                    }
                    ImGui::PopStyleColor();
                    ImGui::Text("%s", texture->name().c_str());
                    ImGui::EndGroup();
                }
            }
        }
        ImGui::EndChild();

        if (ImGui::Button("Close", ImVec2(120, 30)))
        {
            state.modalActive = false;
            ImGui::CloseCurrentPopup();
        }
        ImGui::EndPopup();
    }

    return result;
}
