#include "program.h"
#include <imgui.h>

void Program::renderGuiToolbar()
{
    ImGui::Begin("toolbar", &state.show_toolbar, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar);
    {
        ImGui::SetWindowPos(ImVec2(0, 22));
        ImGui::SetWindowSize(ImVec2(float(toolButtonBarWidth), state.height - 57));

        for (int i = 0; i < tools.toolCount(); i++)
        {
            if (tools[i]._icon == nullptr)
            {
                ImGui::Separator();
                continue;
            }
            ImGui::PushID(i);
            ImGui::PushStyleColor(ImGuiCol_Button, tools.isSelected(i) ? ImVec4(0.0f, 0.2f, 0.8f, 1.0f) : ImGui::GetStyle().Colors[ImGuiCol_Button]);
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, tools.isSelected(i) ? ImVec4(0.0f, 0.2f, 0.6f, 1.0f) : ImGui::GetStyle().Colors[ImGuiCol_ButtonHovered]);
            if (ImGui::Button(tools[i]._icon, ImVec2(50, 50)))
            {
                tools.selectTool(i, state);
            }
            if (ImGui::IsItemHovered())
            {
                ImGui::SetTooltip("%s", tools[i]._tooltip.c_str());
            }
            ImGui::PopStyleColor(2);
            ImGui::PopID();
        }
    }
    ImGui::End();
}
