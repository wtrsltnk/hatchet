#include "program.h"

#include "actions/baseaction.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/string_cast.hpp>
#include <imgui.h>
#include <iostream>
#include <sstream>

glm::vec2 ImGuiGlobalToViewLocal(
    const ImVec2 &v,
    Camera *view,
    const app_state &state)
{
    return glm::vec2(
        v.x - view->ViewRect().x,
        (state.height - v.y) - view->ViewRect().y);
}

glm::vec2 ScreenToView(
    glm::vec4 const &viewRect,
    glm::vec2 const &point)
{
    return glm::vec2(point.x - viewRect.x, viewRect.w - (point.y - viewRect.y));
}

void Program::renderGuiViews()
{
    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4());
    {
        ImGui::Begin("content", &(state.show_content), ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoScrollbar);
        {
            ImGui::SetWindowPos(ImVec2(toolButtonBarWidth - 8.0f, 14.0f));
            ImGui::SetWindowSize(ImVec2(state.width - toolButtonBarWidth - dockbarWidth + 12, state.height - 14 - statusBarHeight));

            auto cursor = ImGui::GetCursorPos();

            ImGui::PushID(1);
            renderMapView(ViewNames::BottomLeft);
            ImGui::PopID();

            ImGui::PushID(2);
            renderMapView(ViewNames::TopLeft);
            ImGui::PopID();

            ImGui::PushID(3);
            renderMapView(ViewNames::BottomRight);
            ImGui::PopID();

            ImGui::PushID(4);
            renderMapView(ViewNames::TopRight);
            ImGui::PopID();

            ImGui::SetCursorPos(cursor);
            ImGui::Dummy(ImVec2(state.width - toolButtonBarWidth - dockbarWidth, state.height - 57));

            if (ImGui::BeginPopupContextItem("item context menu"))
            {
                if (ImGui::Selectable("2D top (x/y)"))
                {
                    state.views[state.focusedView]->SetViewType(ViewTypes::Top);
                }
                if (ImGui::Selectable("2D front (y/z)"))
                {
                    state.views[state.focusedView]->SetViewType(ViewTypes::Front);
                }
                if (ImGui::Selectable("2D side (x/z)"))
                {
                    state.views[state.focusedView]->SetViewType(ViewTypes::Side);
                }
                ImGui::Separator();
                if (ImGui::Selectable("3D wireframe"))
                {
                    state.views[state.focusedView]->SetViewType(ViewTypes::PerspectiveWireframe);
                }
                if (ImGui::Selectable("3D flat"))
                {
                    state.views[state.focusedView]->SetViewType(ViewTypes::PerspectiveFlat);
                }
                if (ImGui::Selectable("3D textured"))
                {
                    state.views[state.focusedView]->SetViewType(ViewTypes::PerspectiveTextured);
                }
                ImGui::Separator();
                if (ImGui::Selectable("Paste"))
                {
                    pasteInView();
                }
                if (ImGui::Selectable("Paste Special"))
                {
                    pasteSpecialInView();
                }
                if (ImGui::Selectable("Export Again"))
                {
                    exportMapAgain();
                }
                ImGui::Separator();
                if (ImGui::Selectable("Undo"))
                {
                    undo();
                }
                if (ImGui::Selectable("Redo"))
                {
                    redo();
                }
                ImGui::EndPopup();
            }

            //            static bool showcam;
            //            ImGui::Begin("cam", &showcam);
            //            ImGui::Text("cam");
            //            if(state.views.find(state.focusedView) != state.views.end()
            //                    && state.views[state.focusedView]->IsPerspectiveViewType())
            //            {
            //                auto fps = static_cast<FpsCamera*>(state.views[state.focusedView]);
            //                float p = fps->Pitch();
            //                float r = fps->Roll();
            //                float y = fps->Yaw();
            //                ImGui::SliderFloat("Pitch", &p, glm::radians(-360.0f), glm::radians(360.0f));
            //                ImGui::SliderFloat("Roll", &r, glm::radians(-360.0f), glm::radians(360.0f));
            //                ImGui::SliderFloat("Yaw", &y, glm::radians(-360.0f), glm::radians(360.0f));
            //                fps->SetPitch(p);
            //                fps->SetRoll(r);
            //                fps->SetYaw(y);
            //                fps->UpdateView();
            //            }
            //            ImGui::End();
        }
        ImGui::End();
    }
    ImGui::PopStyleColor();

    if (tools.selectedTool()._actionFactory != nullptr)
    {
        tools.selectedTool()._actionFactory->RenderTool();
    }
}

void Program::renderGrid(
    Camera *viewstate,
    const glm::mat4 &projection,
    const glm::mat4 &view,
    const glm::mat4 &model)
{
    if (!viewstate->IsPerspectiveViewType())
    {
        auto gridView = view;
        if (viewstate->ViewType() == ViewTypes::Front)
        {
            gridView = glm::rotate(gridView, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        }
        else if (viewstate->ViewType() == ViewTypes::Top)
        {
            gridView = glm::rotate(gridView, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        }

        state.gridVertexBuffer.BindForRendering(projection, gridView, model, 6);

        glm::vec3 scale, translation, skew;
        glm::vec4 perspective;
        glm::quat orientation;
        glm::decompose(gridView, scale, orientation, translation, skew, perspective);

        if (glm::length(scale) > 1.15f || state.gridSize > 128)
        {
            state.gridVertexBuffer.SetBrushColor(glm::vec4(0.4f, 0.4f, 0.4f, 0.7f));
            glDrawArrays(GL_LINES, 0, state.gridVertexBuffer.vertCount);
        }
        else
        {
            state.gridVertexBuffer.SetBrushColor(glm::vec4(0.4f, 0.4f, 0.4f, 0.5f));
            glDrawArrays(GL_LINES, 0, state.gridPartialBufferSize);
        }

        state.gridVertexBuffer.SetBrushColor(glm::vec4(0.0f, 0.4f, 0.6f, 1.0f));
        glDrawArrays(GL_LINES, 0, 4);

        state.gridVertexBuffer.Unbind();
    }
}

void Program::renderMapView(
    ViewNames v)
{
    if (state.views.find(v) == state.views.end())
    {
        return;
    }

    auto viewstate = state.views[v];

    auto viewPosition = ImVec2(
        viewstate->ViewRect().x - toolButtonBarWidth + 8,
        state.height - viewstate->ViewRect().y - viewstate->ViewRect().w - 14);

    auto viewSize = ImVec2(
        viewstate->ViewRect().z,
        viewstate->ViewRect().w);

    ImGui::SetCursorPos(viewPosition);
    ImGui::InvisibleButton("SelectInViewButton", viewSize);

    if (ImGui::IsItemHovered(ImGuiHoveredFlags_Default) && tools.selectedTool()._actionFactory != nullptr)
    {
        if (ImGui::IsMouseClicked(0))
        {
            tools.selectedTool()._actionFactory->PrimaryMouseButtonDown(
                ImGui::GetMousePos().x,
                ImGui::GetMousePos().y,
                ImGui::GetIO().KeyShift,
                ImGui::GetIO().KeyCtrl,
                ImGui::GetIO().KeyAlt,
                ImGui::GetIO().KeySuper);

            state.selectingView = state.focusedView;
        }

        if (ImGui::IsMouseClicked(1))
        {
            tools.selectedTool()._actionFactory->SecondaryMouseButtonDown(
                ImGui::GetMousePos().x,
                ImGui::GetMousePos().y,
                ImGui::GetIO().KeyShift,
                ImGui::GetIO().KeyCtrl,
                ImGui::GetIO().KeyAlt,
                ImGui::GetIO().KeySuper);
        }

        if (ImGui::IsMouseDragging(0) && state.selectingView == state.focusedView)
        {
            tools.selectedTool()._actionFactory->MouseMove(
                int(ImGui::GetMousePos().x),
                int(ImGui::GetMousePos().y));
        }

        if (ImGui::IsMouseReleased(0))
        {
            tools.selectedTool()._actionFactory->PrimaryMouseButtonUp(
                ImGui::GetMousePos().x,
                ImGui::GetMousePos().y,
                ImGui::GetIO().KeyShift,
                ImGui::GetIO().KeyCtrl,
                ImGui::GetIO().KeyAlt,
                ImGui::GetIO().KeySuper);

            state.selectingView = ViewNames::Unknown;
        }

        if (ImGui::IsMouseReleased(1))
        {
            tools.selectedTool()._actionFactory->SecondaryMouseButtonUp(
                ImGui::GetMousePos().x,
                ImGui::GetMousePos().y,
                ImGui::GetIO().KeyShift,
                ImGui::GetIO().KeyCtrl,
                ImGui::GetIO().KeyAlt,
                ImGui::GetIO().KeySuper);

            state.selectingView = ViewNames::Unknown;
        }
    }

    glViewport(viewstate->ViewRect().x, viewstate->ViewRect().y, viewstate->ViewRect().z, viewstate->ViewRect().w);

    glScissor(viewstate->ViewRect().x, viewstate->ViewRect().y, viewstate->ViewRect().z, viewstate->ViewRect().w);
    glEnable(GL_SCISSOR_TEST);

    glClearColor(14 / 255.0f, 44 / 255.0f, (state.focusedView == v ? 60 : 54) / 255.0f, 155 / 255.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto projection = viewstate->ProjectionMatrix();
    auto view = viewstate->ViewMatrix();
    auto model = glm::mat4(1.0f);

    if (viewstate->ViewType() == ViewTypes::Side)
    {
        view = glm::rotate(view, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    }
    else if (viewstate->ViewType() == ViewTypes::Front)
    {
        view = glm::rotate(view, glm::radians(90.0f), glm::vec3(0.0f, -1.0f, 0.0f));
        view = glm::rotate(view, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    }

    renderGrid(viewstate, projection, view, model);

    if (state.scene._flags & SceneFlags::Modified)
    {
        state.renderData.Collect(state.scene);
        state.vertexBuffer.UploadVertexData(state.renderData._verts);
        state.scene._flags &= ~SceneFlags::Modified;
    }

    if (state.renderData._verts.size() > 0)
    {
        if (!viewstate->IsPerspectiveViewType() || viewstate->ViewType() == ViewTypes::PerspectiveWireframe)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glDisable(GL_DEPTH_TEST);
        }

        GLenum mode = viewstate->IsPerspectiveViewType() ? GL_TRIANGLE_FAN : GL_LINE_LOOP;

        state.vertexBuffer.BindForRendering(projection, view, model, int(viewstate->ViewType()));

        for (const auto &t : state.renderData._data)
        {
            for (const MapFace &face : t.second)
            {
                // Render selected faces red
                if (state.selectedBrushes.find(face._brushId) != state.selectedBrushes.end())
                {
                    state.vertexBuffer.SetBrushColor(glm::vec4(1, 0, 0, 1));
                }
                else
                {
                    state.vertexBuffer.SetBrushColor(face._color);
                }
                glDrawArrays(mode, face._firstVertex, face._vertexCount);
            }
        }

        // Now render the yellow wireframe around the selected items, byt only in perspective view
        if (viewstate->IsPerspectiveViewType())
        {
            glDisable(GL_DEPTH_TEST);
            for (const auto &t : state.renderData._data)
            {
                for (const MapFace &face : t.second)
                {
                    if (state.selectedBrushes.find(face._brushId) == state.selectedBrushes.end())
                    {
                        continue;
                    }

                    state.vertexBuffer.SetBrushColor(glm::vec4(1, 1, 0, 1));
                    glDrawArrays(GL_LINE_LOOP, face._firstVertex, face._vertexCount);
                }
            }
        }

        state.vertexBuffer.Unbind();

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glEnable(GL_DEPTH_TEST);
    }

    glDisable(GL_SCISSOR_TEST);

    ImGui::SetCursorPos(ImVec2(viewstate->ViewRect().x - toolButtonBarWidth + 22, state.height - viewstate->ViewRect().y - menuBarHeight - 80));
    if (viewstate->ViewType() == ViewTypes::Front)
    {
        ImGui::Image((ImTextureID)frontAxis, ImVec2(80, 80));
        if (ImGui::IsItemHovered())
        {
            ImGui::SetTooltip("Frontview");
        }
    }
    if (viewstate->ViewType() == ViewTypes::Side)
    {
        ImGui::Image((ImTextureID)sideAxis, ImVec2(80, 80));
        if (ImGui::IsItemHovered())
        {
            ImGui::SetTooltip("Sideview");
        }
    }
    if (viewstate->ViewType() == ViewTypes::Top)
    {
        ImGui::Image((ImTextureID)topAxis, ImVec2(80, 80));
        if (ImGui::IsItemHovered())
        {
            ImGui::SetTooltip("Topview");
        }
    }
}

void Program::pasteInView() {}

void Program::pasteSpecialInView() {}
