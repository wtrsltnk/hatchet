#include "program.h"
#include "actions/baseaction.h"
#include "fpscamera.h"
#include "orthocamera.h"

#include <glad/glad.h>
#include <imgui.h>
#include <imgui_impl_glfw_gl3.h>
#include <imgui_internal.h>
#include <iostream>

#define IMGUI_TABS_IMPLEMENTATION
#include "imgui_tabs.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <streambuf>
#include <string>
#include <vector>

static std::string mapContent = "{ \
    \"classname\" \"worldspawn\" \
    \"message\" \"DUST by Dave Johnston - for CounterStrike\" \
    \"skyname\" \"des\" \
    \"MaxRange\" \"8000\" \
    \"classname\" \"worldspawn\" \
    \"mapversion\" \"220\" \
    \"wad\" \"\\program files\\valve\\half-life\\valve\\halflife.wad;\\program files\\valve\\half-life\\cstrike\\cs_dust.wad\" \
    { \
        ( 256 -640 -192 ) ( 0 -640 -192 ) ( 0 0 0 ) SANDROAD [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1 \
        ( 256 0 -64 ) ( 0 0 -64 ) ( 0 -640 -256 ) SANDROAD [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1 \
        ( 0 -640 -256 ) ( 0 0 -64 ) ( 0 0 0 ) SANDROAD [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1 \
        ( 256 -640 -192 ) ( 256 0 0 ) ( 256 0 -64 ) SANDROAD [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1 \
        ( 0 0 -64 ) ( 256 0 -64 ) ( 256 0 0 ) SANDROAD [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1 \
        ( 0 -640 -192 ) ( 256 -640 -192 ) ( 256 -640 -256 ) SANDROAD [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1 \
    } \
}";

Program::Program(
    GLFWwindow *window)
    : _window(window)
{
    glfwSetWindowUserPointer(this->_window, static_cast<void *>(this));

    options.hlExecutablePath[0] = '\0';
    options.mod[0] = '\0';
}

Program::~Program()
{
    glfwSetWindowUserPointer(this->_window, nullptr);
}

void Program::addTexturesFromPath(
    std::filesystem::path const &path)
{
    for (const auto &file : std::filesystem::directory_iterator(path))
    {
        if (file.is_directory())
        {
            continue;
        }

        if (file.path().extension() == ".wad")
        {
            textures.addTexturesFromWadFile(file.path().string());
        }
    }
}

void Program::populateTextureManagerFromOptions()
{
    if (options.hlExecutablePath.empty())
    {
        return;
    }

    auto hl = std::filesystem::path(options.hlExecutablePath);
    auto hlPath = hl.parent_path();
    auto valvePath = hlPath / "valve";

    if (std::filesystem::exists(valvePath))
    {
        addTexturesFromPath(valvePath);
    }

    if (std::string(options.mod) != "")
    {
        auto modPath = hlPath / options.mod;
        if (std::filesystem::exists(modPath))
        {
            addTexturesFromPath(modPath);
        }
    }

    state.foundTextures = textures.findTextures("");
}

GLuint loadTexture(
    std::string const &filename)
{
    int x, y, comp;
    auto pixels = stbi_load(filename.c_str(), &x, &y, &comp, 4);
    if (pixels == nullptr)
    {
        return 0;
    }

    auto textureSize = glm::vec2(x, y);

    GLuint _textureId;
    glGenTextures(1, &_textureId);

    glBindTexture(GL_TEXTURE_2D, _textureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, comp == 4 ? GL_RGBA : GL_RGB, static_cast<GLsizei>(textureSize.x), static_cast<GLsizei>(textureSize.y), 0, comp == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, pixels);

    glBindTexture(GL_TEXTURE_2D, 0);
    delete[] pixels;

    return _textureId;
}

bool Program::SetUp()
{
    _userInput.ReadKeyMappings(std::filesystem::path(_settingsDir) / KEYMAP_FILE);

    sideAxis = loadTexture("../hatchet/side.png");
    frontAxis = loadTexture("../hatchet/front.png");
    topAxis = loadTexture("../hatchet/top.png");

    populateTextureManagerFromOptions();

    state.splitX = int((state.width - dockbarWidth - toolButtonBarWidth) / 2);
    state.splitY = int((state.height - menuBarHeight - statusBarHeight) / 2);

    state.views.insert(std::make_pair(ViewNames::TopLeft, new FpsCamera(ViewTypes::PerspectiveFlat)));
    state.views[ViewNames::TopLeft]->SetViewRect(GetViewRect(ViewNames::TopLeft));

    state.views.insert(std::make_pair(ViewNames::BottomLeft, new OrthoCamera(ViewTypes::Side)));
    state.views[ViewNames::BottomLeft]->SetViewRect(GetViewRect(ViewNames::BottomLeft));

    state.views.insert(std::make_pair(ViewNames::BottomRight, new OrthoCamera(ViewTypes::Front)));
    state.views[ViewNames::BottomRight]->SetViewRect(GetViewRect(ViewNames::BottomRight));

    state.views.insert(std::make_pair(ViewNames::TopRight, new OrthoCamera(ViewTypes::Top)));
    state.views[ViewNames::TopRight]->SetViewRect(GetViewRect(ViewNames::TopRight));

    hresizeCursor = glfwCreateStandardCursor(GLFW_HRESIZE_CURSOR);
    vresizeCursor = glfwCreateStandardCursor(GLFW_VRESIZE_CURSOR);
    crosshairCursor = glfwCreateStandardCursor(GLFW_CROSSHAIR_CURSOR);

    ImGuiIO &io = ImGui::GetIO();
    if (io.Fonts->AddFontFromFileTTF("../hatchet/imgui/extra_fonts/Roboto-Medium.ttf", 16.0f) == nullptr)
    {
        return false;
    }

    ImFontConfig config;
    config.MergeMode = true;

    static const ImWchar icons_ranges_fontawesome[] = {0xf000, 0xf3ff, 0};
    if (io.Fonts->AddFontFromFileTTF("../hatchet/fontawesome-webfont.ttf", 22.0f, &config, icons_ranges_fontawesome) == nullptr)
    {
        return false;
    }

    static const ImWchar icons_ranges_googleicon[] = {0xe000, 0xeb4c, 0};
    if (io.Fonts->AddFontFromFileTTF("../hatchet/MaterialIcons-Regular.ttf", 24.0f, &config, icons_ranges_googleicon) == nullptr)
    {
        return false;
    }

    state.vertexBuffer.Setup("mapShader");

    loadMap("../hatchet/views.map");

    int w = 128;
    int h = 128;
    std::vector<unsigned char> img;
    for (int j = 0; j < h; ++j)
    {
        for (int i = 0; i < w; ++i)
        {
            img.push_back(i < w / 32 || j < h / 32 ? 255 : 0);
        }
    }

    glGenTextures(1, &_gridTexture);

    glBindTexture(GL_TEXTURE_2D, _gridTexture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, static_cast<GLsizei>(w), static_cast<GLsizei>(h), 0, GL_RED, GL_UNSIGNED_BYTE, img.data());
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    setupGridVertexArray(state.gridSize);

    tools.selectTool(0, state);

    return true;
}

void Program::setupGridVertexArray(
    int gridSize)
{
    std::vector<MapVertex> gridData;

    int max = 2048, min = -max;
    gridData.push_back(MapVertex(glm::vec3(min, 0, 0), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
    gridData.push_back(MapVertex(glm::vec3(max, 0, 0), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
    gridData.push_back(MapVertex(glm::vec3(0, 0, min), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
    gridData.push_back(MapVertex(glm::vec3(0, 0, max), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
    for (int y = min; y <= max; y++)
    {
        if (y % (gridSize * 8) != 0) continue;

        gridData.push_back(MapVertex(glm::vec3(min, 0, y), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
        gridData.push_back(MapVertex(glm::vec3(max, 0, y), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
    }

    for (int x = min; x <= max; x++)
    {
        if (x % (gridSize * 8) != 0) continue;

        gridData.push_back(MapVertex(glm::vec3(x, 0, min), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
        gridData.push_back(MapVertex(glm::vec3(x, 0, max), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
    }
    state.gridPartialBufferSize = gridData.size();

    for (int y = min; y <= max; y++)
    {
        if (y % gridSize != 0) continue;

        gridData.push_back(MapVertex(glm::vec3(min, 0, y), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0)));
        gridData.push_back(MapVertex(glm::vec3(max, 0, y), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0)));
    }

    for (int x = min; x <= max; x++)
    {
        if (x % gridSize != 0) continue;

        gridData.push_back(MapVertex(glm::vec3(x, 0, min), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0)));
        gridData.push_back(MapVertex(glm::vec3(x, 0, max), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0)));
    }

    state.gridVertexBuffer.Setup("mapShader");
    state.gridVertexBuffer.UploadVertexData(gridData);
}

void Program::onResize(
    int width,
    int height)
{
    auto dtw = float(width - dockbarWidth - toolButtonBarWidth) / float(state.width - dockbarWidth - toolButtonBarWidth);
    auto dth = float(height - menuBarHeight - statusBarHeight) / float(state.height - menuBarHeight - statusBarHeight);

    state.width = float(width);
    state.height = float(height);

    state.splitX = int(state.splitX * dtw);
    state.splitY = int(state.splitY * dth);

    UpdateViewRects();
}

void Program::Render()
{
    static double lastTime = glfwGetTime();
    double currTime = glfwGetTime();
    double dt = currTime - lastTime;
    lastTime = currTime;

    if (state.views.find(state.focusedView) != state.views.end())
    {
        glm::vec2 move(0.0f, 0.0f);
        if (_userInput.ActionState(UserInputActions::PerspectiveCameraForward))
        {
            move.y = float(dt);
        }
        else if (_userInput.ActionState(UserInputActions::PerspectiveCameraBackward))
        {
            move.y = -float(dt);
        }

        if (_userInput.ActionState(UserInputActions::PerspectiveCameraLeft))
        {
            move.x = float(dt);
        }
        else if (_userInput.ActionState(UserInputActions::PerspectiveCameraRight))
        {
            move.x = -float(dt);
        }

        double speed = 500.0;
        state.views[state.focusedView]->Move(move * float(speed));
    }

    glViewport(0, 0, int(state.width), int(state.height));
    glClearColor(114 / 255.0f, 144 / 255.0f, 154 / 255.0f, 255 / 255.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    ImGui_ImplGlfwGL3_NewFrame();

    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
    {
        renderGuiMenu();

        ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.20f, 0.20f, 0.47f, 0.60f));
        {
            renderGuiToolbar();

            renderGuiViews();

            renderGuiDockbar();

            renderGuiStatusbar();
        }
        ImGui::PopStyleColor();
    }
    ImGui::PopStyleVar();

    ImGui::Render();
}

bool rectContains(
    glm::vec4 const &rect,
    glm::vec2 const &point)
{
    if (point.x < rect.x || point.y < rect.y)
    {
        return false;
    }

    if (point.x > (rect.x + rect.z) || point.y > (rect.y + rect.w))
    {
        return false;
    }

    return true;
}

ViewNames Program::getViewByMousePos(
    int x,
    int y)
{
    if (rectContains(GetViewRect(ViewNames::BottomLeft), glm::vec2(x, state.height - y)))
    {
        return ViewNames::BottomLeft;
    }

    if (rectContains(GetViewRect(ViewNames::TopLeft), glm::vec2(x, state.height - y)))
    {
        return ViewNames::TopLeft;
    }

    if (rectContains(GetViewRect(ViewNames::BottomRight), glm::vec2(x, state.height - y)))
    {
        return ViewNames::BottomRight;
    }

    if (rectContains(GetViewRect(ViewNames::TopRight), glm::vec2(x, state.height - y)))
    {
        return ViewNames::TopRight;
    }

    return ViewNames::Unknown;
}

void Program::onKeyAction(
    int key,
    int scancode,
    int action,
    int mods)
{
    (void)scancode;

    state.shiftPressed = (mods & GLFW_MOD_SHIFT);
    state.ctrlPressed = (mods & GLFW_MOD_CONTROL);

    if (action == GLFW_PRESS || action == GLFW_RELEASE)
    {
        UserInputEvent uie = {GLFW_PRESS, key};
        _userInput.ProcessEvent(uie, (action == GLFW_PRESS));
    }

    if (state.views.find(state.focusedView) == state.views.end())
    {
        return;
    }

    if (action == GLFW_PRESS || action == GLFW_REPEAT)
    {
        auto c = ImGuiGlobalToViewLocal(ImGui::GetMousePos(), state.views[state.focusedView], state);
        if (key == GLFW_KEY_MINUS || key == GLFW_KEY_KP_SUBTRACT)
        {
            ZoomOutView(state.focusedView, c);
        }
        else if (key == GLFW_KEY_KP_ADD || key == '+')
        {
            ZoomInView(state.focusedView, c);
        }
        else if (key == 'Z')
        {
            ToggleLookMode(state.focusedView);
        }
        else if (key == GLFW_KEY_ESCAPE)
        {
            CancelLookMode(state.focusedView);
        }
        else if (key == GLFW_KEY_LEFT_BRACKET)
        {
            if (state.gridSize < 512)
            {
                state.gridSize *= 2;

                setupGridVertexArray(state.gridSize);
            }
        }
        else if (key == GLFW_KEY_RIGHT_BRACKET)
        {
            if (state.gridSize > 1)
            {
                state.gridSize /= 2;

                setupGridVertexArray(state.gridSize);
            }
        }
    }
}

/// Calculates the rectanlge of a view from the bottom left corner of the window
glm::vec4 Program::GetViewRect(
    ViewNames view)
{
    glm::vec4 rect;

    if (view == ViewNames::BottomLeft)
    {
        // bottom left
        rect = glm::vec4(toolButtonBarWidth,
                         statusBarHeight,
                         state.splitX - 2,
                         state.splitY - 2);
    }
    else if (view == ViewNames::TopLeft)
    {
        // top left
        rect = glm::vec4(toolButtonBarWidth,
                         statusBarHeight + state.splitY + 2,
                         state.splitX - 2,
                         state.height - menuBarHeight - statusBarHeight - state.splitY - 2);
    }
    else if (view == ViewNames::BottomRight)
    {
        // bottom right
        rect = glm::vec4(toolButtonBarWidth + state.splitX + 2, statusBarHeight,
                         state.width - toolButtonBarWidth - state.splitX - dockbarWidth - 2,
                         state.splitY - 2);
    }
    else if (view == ViewNames::TopRight)
    {
        // top right
        rect = glm::vec4(toolButtonBarWidth + state.splitX + 2,
                         statusBarHeight + state.splitY + 2,
                         state.width - toolButtonBarWidth - state.splitX - dockbarWidth - 2,
                         state.height - menuBarHeight - statusBarHeight - state.splitY - 2);
    }

    return rect;
}

void Program::UpdateViewRects()
{
    if (state.views.find(ViewNames::TopLeft) != state.views.end())
    {
        state.views[ViewNames::TopLeft]
            ->SetViewRect(GetViewRect(ViewNames::TopLeft));
    }
    if (state.views.find(ViewNames::BottomLeft) != state.views.end())
    {
        state.views[ViewNames::BottomLeft]
            ->SetViewRect(GetViewRect(ViewNames::BottomLeft));
    }
    if (state.views.find(ViewNames::BottomRight) != state.views.end())
    {
        state.views[ViewNames::BottomRight]
            ->SetViewRect(GetViewRect(ViewNames::BottomRight));
    }
    if (state.views.find(ViewNames::TopRight) != state.views.end())
    {
        state.views[ViewNames::TopRight]
            ->SetViewRect(GetViewRect(ViewNames::TopRight));
    }
}

void Program::SetZoom(
    ViewNames view,
    float multiplier,
    const glm::vec2 &zoomAt)
{
    if (state.views.find(view) == state.views.end())
    {
        return;
    }

    // Perspective camera does not have a zoom
    if (state.views[view]->IsPerspectiveViewType())
    {
        auto fpsCamera = static_cast<FpsCamera *>(state.views[view]);

        if (multiplier < 1.0f)
        {
            fpsCamera->Move(glm::vec3(0.0f, -20.0f, 0.0f));
        }
        else
        {
            fpsCamera->Move(glm::vec3(0.0f, 20.0f, 0.0f));
        }
        return;
    }

    auto camera = static_cast<OrthoCamera *>(state.views[view]);

    auto origin = glm::vec3(
        zoomAt.x,
        zoomAt.y,
        0.0f);

    auto mouseInWorld = glm::unProject(
        origin,
        camera->ViewMatrix(),
        camera->ProjectionMatrix(),
        glm::vec4(0.0f, 0.0f, camera->ViewRect().z, camera->ViewRect().w));

    camera->Zoom(multiplier, mouseInWorld);
}

const float zoomMultiplier = 1.1f;

void Program::ZoomOutView(
    ViewNames view,
    const glm::vec2 &zoomAt)
{
    if (view == ViewNames::Unknown)
    {
        return;
    }

    SetZoom(view, 1.0f / zoomMultiplier, zoomAt);
}

void Program::ZoomInView(
    ViewNames view,
    const glm::vec2 &zoomAt)
{
    if (view == ViewNames::Unknown)
    {
        return;
    }

    SetZoom(view, zoomMultiplier, zoomAt);
}

void Program::CancelLookMode(
    ViewNames view)
{
    if (view == ViewNames::Unknown)
    {
        return;
    }

    if (state.views[view]->ViewType() == ViewTypes::Front)
    {
        return;
    }

    if (state.views[view]->ViewType() == ViewTypes::Side)
    {
        return;
    }

    if (state.views[view]->ViewType() == ViewTypes::Top)
    {
        return;
    }

    state.views[view]->SetMouseLookMode(false);
}

void Program::ToggleLookMode(
    ViewNames view)
{
    if (view == ViewNames::Unknown)
    {
        return;
    }

    if (state.views[view]->ViewType() == ViewTypes::Front)
    {
        return;
    }

    if (state.views[view]->ViewType() == ViewTypes::Side)
    {
        return;
    }

    if (state.views[view]->ViewType() == ViewTypes::Top)
    {
        return;
    }

    state.views[view]->SetMouseLookMode(!state.views[view]->MouseLookMode());

    CenterCursorToView(view);
}

void Program::OnViewChanged(
    ViewNames oldView,
    ViewNames newView)
{
    (void)oldView;
    (void)newView;
}

void Program::CenterCursorToView(
    ViewNames view)
{
    glm::vec4 rect = GetViewRect(view);

    state.mousex = int(rect.x + (rect.z / 2.0f));
    state.mousey = int(state.height - (rect.y + (rect.w / 2.0f)));

    glfwSetCursorPos(this->_window, state.mousex, state.mousey);
}

void Program::onMouseMove(
    int x,
    int y)
{
    if (!state.modalActive)
    {
        auto newView = getViewByMousePos(x, y);
        if (newView != ViewNames::Unknown)
        {
            if (newView != state.focusedView)
            {
                OnViewChanged(state.focusedView, newView);
            }
            state.focusedView = newView;
        }
    }

    if (state.views.count(state.focusedView) == 0)
    {
        return;
    }

    if (state.views[state.focusedView]->MouseLookMode())
    {
        auto fpsCamera = static_cast<FpsCamera *>(state.views[state.focusedView]);

        auto diffx = x - state.mousex;
        auto diffy = y - state.mousey;

        fpsCamera->SetPitch(fpsCamera->Pitch() - diffy / 100.0f);
        fpsCamera->SetYaw(fpsCamera->Yaw() + diffx / 100.0f);
        fpsCamera->UpdateView();

        CenterCursorToView(state.focusedView);

        return;
    }

    state.mousex = x;
    state.mousey = y;

    if (!state.modalActive && x > toolButtonBarWidth && x < state.width - dockbarWidth && y > menuBarHeight && y < state.height - statusBarHeight)
    {
        if (!state.dragSplitX && !state.dragSplitY)
        {
            if (glm::abs(x - toolButtonBarWidth - state.splitX) < 5.0f && glm::abs((state.height - y) - statusBarHeight - state.splitY) < 5.0f)
            {
                glfwSetCursor(_window, crosshairCursor);
            }
            else if (glm::abs(x - toolButtonBarWidth - state.splitX) < 5.0f)
            {
                glfwSetCursor(_window, hresizeCursor);
            }
            else if (glm::abs((state.height - y) - statusBarHeight - state.splitY) < 5.0f)
            {
                glfwSetCursor(_window, vresizeCursor);
            }
            else
            {
                glfwSetCursor(_window, NULL);
            }
        }

        if (state.dragSplitX)
        {
            state.splitX = x - toolButtonBarWidth;
        }

        if (state.dragSplitY)
        {
            state.splitY = int(state.height - y) - statusBarHeight;
        }

        if (state.dragSplitX || state.dragSplitY)
        {
            UpdateViewRects();
        }
    }
    else
    {
        glfwSetCursor(_window, NULL);
    }
}

void Program::onMouseButton(
    int button,
    int action,
    int mods)
{
    if (!state.modalActive && action == GLFW_PRESS)
    {
        state.dragSplitX = (glm::abs(state.mousex - toolButtonBarWidth - state.splitX) < 5.0f);
        state.dragSplitY = (glm::abs((state.height - state.mousey) - statusBarHeight - state.splitY) < 5.0f);
    }
    else if (!state.modalActive && action == GLFW_RELEASE)
    {
        state.dragSplitX = false;
        state.dragSplitY = false;
    }
}

void Program::onScroll(
    int x,
    int y)
{
    (void)x;

    if (state.shiftPressed)
    {
    }
    else if (state.ctrlPressed)
    {
    }
    else
    {
        if (state.focusedView != ViewNames::Unknown && !state.modalActive)
        {
            auto c = ImGuiGlobalToViewLocal(ImGui::GetMousePos(), state.views[state.focusedView], state);
            if (y < 0)
            {
                ZoomOutView(state.focusedView, c);
            }
            else
            {
                ZoomInView(state.focusedView, c);
            }
        }
    }
}

void Program::CleanUp() {}

void Program::KeyActionCallback(
    GLFWwindow *window,
    int key,
    int scancode,
    int action,
    int mods)
{
    ImGui_ImplGlfwGL3_KeyCallback(window, key, scancode, action, mods);

    auto app = static_cast<Program *>(glfwGetWindowUserPointer(window));

    if (app != nullptr)
    {
        app->onKeyAction(key, scancode, action, mods);
    }
}

void Program::CursorPosCallback(
    GLFWwindow *window,
    double x,
    double y)
{
    auto app = static_cast<Program *>(glfwGetWindowUserPointer(window));

    if (app != nullptr)
    {
        app->onMouseMove(int(x), int(y));
    }
}

void Program::ScrollCallback(
    GLFWwindow *window,
    double x,
    double y)
{
    ImGui_ImplGlfwGL3_ScrollCallback(window, x, y);

    auto app = static_cast<Program *>(glfwGetWindowUserPointer(window));

    if (app != nullptr)
    {
        app->onScroll(int(x), int(y));
    }
}

void Program::MouseButtonCallback(
    GLFWwindow *window,
    int button,
    int action,
    int mods)
{
    auto app = static_cast<Program *>(glfwGetWindowUserPointer(window));

    if (app != nullptr)
    {
        app->onMouseButton(button, action, mods);
    }
}

void Program::ResizeCallback(
    GLFWwindow *window,
    int width,
    int height)
{
    auto app = static_cast<Program *>(glfwGetWindowUserPointer(window));

    if (app != nullptr)
    {
        app->onResize(width, height);
    }
}
