#ifndef PROGRAM_H
#define PROGRAM_H

#include "camera.h"
#include "geometry/GeometryQueryService.h"
#include "geometry/mapscene.h"
#include "geometry/selectioncommandservice.h"
#include "texturemanager.h"
#include "tools.h"
#include "userinput.h"
#include "vertexbuffer.h"

#include <GLFW/glfw3.h>
#include <filesystem>
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <set>

#include <imgui.h>
#include <viewnames.h>

#define KEYMAP_FILE "hatchet.keymap"

glm::vec2 ImGuiGlobalToViewLocal(
    const ImVec2 &v,
    Camera *view,
    const app_state &state);

struct view_state
{
    bool lookMode = false;
    Camera *camera;
};

struct app_state
{
    bool show_toolbar = false;
    bool show_content = false;
    bool show_dockbar = false;
    bool show_texture_browser = false;
    float width = 200.0f;
    float height = 300.0f;
    int mousex = 0;
    int mousey = 0;
    int gridSize = 8;
    bool shiftPressed = false;
    bool ctrlPressed = false;
    int splitX = 100;
    int splitY = 150;
    bool dragSplitX = false;
    bool dragSplitY = false;
    bool modalActive = false;
    std::map<ViewNames, Camera *> views;
    ViewNames focusedView = ViewNames::Unknown;
    ViewNames selectingView = ViewNames::Unknown;

    MapScene scene;
    std::set<long> selectedBrushes;
    std::set<long> selectedEntities;
    GeometryQueryService renderData;
    SelectionCommandService selectionService;
    VertexBuffer vertexBuffer;
    VertexBuffer gridVertexBuffer;
    size_t gridPartialBufferSize;

    std::map<std::string, std::vector<Texture *>> foundTextures;
    Texture *currentTexture = nullptr;
};

glm::vec2 ImGuiGlobalToViewLocal(const ImVec2 &v, Camera *view, const app_state &state);

struct app_options
{
    std::string hlExecutablePath;
    std::string mod;

    app_options &operator=(app_options const &opt2)
    {
        hlExecutablePath = opt2.hlExecutablePath;
        mod = opt2.mod;

        return *this;
    }
};

class Program
{
public:
    Program(
        GLFWwindow *window);

    virtual ~Program();

    bool SetUp();
    void Render();
    void CleanUp();

public:
    static void KeyActionCallback(
        GLFWwindow *window,
        int key,
        int scancode,
        int action,
        int mods);

    static void ResizeCallback(
        GLFWwindow *window,
        int width,
        int height);

    static void CursorPosCallback(
        GLFWwindow *window,
        double x,
        double y);

    static void ScrollCallback(
        GLFWwindow *window,
        double x,
        double y);

    static void MouseButtonCallback(
        GLFWwindow *window,
        int button,
        int action,
        int mods);

protected:
    void onKeyAction(
        int key,
        int scancode,
        int action,
        int mods);

    void onResize(
        int width,
        int height);

    void onMouseMove(
        int x,
        int y);

    void onMouseButton(
        int button,
        int action,
        int mods);

    void onScroll(
        int x,
        int y);

private:
    GLFWwindow *_window;
    GLFWcursor *hresizeCursor, *vresizeCursor, *crosshairCursor;

    GLuint sideAxis = 0;
    GLuint frontAxis = 0;
    GLuint topAxis = 0;
    GLuint _gridTexture = 0;

    app_state state;
    app_options options;
    Tools tools;
    TextureManager textures;
    std::set<int> pressedKeys;
    std::string _settingsDir;
    UserInput _userInput;

    void loadMap(
        std::string const &filename);

    void newMap();

    void openMap();

    void saveMap();

    void saveMapAs();

    void closeMap();

    void quitApp();

    void exportMap();

    void exportMapAgain();

    void undo();

    void redo();

    void cut();

    void copy();

    void paste();

    void pasteInView();

    void pasteSpecialInView();

    void about();

    void populateTextureManagerFromOptions();

    void addTexturesFromPath(
        std::filesystem::path const &path);

    void setupGridVertexArray(
        int gridSize);

    void renderGuiMenu();

    void renderGuiDockbar();

    void renderGuiToolbar();

    void renderGuiStatusbar();

    void renderGuiViews();

    bool renderGuiOptions(
        app_options &opt);

    void renderGuiAbout();

    Texture *renderGuiTextureBrowser();

    void renderMapView(
        ViewNames v);

    void renderGrid(
        Camera *camera,
        const glm::mat4 &projection,
        const glm::mat4 &view,
        const glm::mat4 &model);

    ViewNames getViewByMousePos(
        int x,
        int y);

    void OnViewChanged(
        ViewNames oldView,
        ViewNames newView);

    glm::vec4 GetViewRect(
        ViewNames view);

    void UpdateViewRects();

    void SetZoom(
        ViewNames view,
        float zoom,
        const glm::vec2 &zoomAt);

    void ZoomOutView(
        ViewNames view,
        const glm::vec2 &zoomAt);

    void ZoomInView(
        ViewNames view,
        const glm::vec2 &zoomAt);

    void ToggleLookMode(
        ViewNames view);

    void CancelLookMode(
        ViewNames view);

    void CenterCursorToView(ViewNames view);

    const int dockbarWidth = 250;
    const int toolButtonBarWidth = 65;
    const int menuBarHeight = 22;
    const int statusBarHeight = 35;

};

#endif // PROGRAM_H
