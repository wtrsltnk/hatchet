#include "tools.h"
#include "font-icons.h"

#include "actions/selectionaction.h"
#include <iostream>

Tools::Tools()
    : _selectedTool(0)
{
    static SelectionActionFactory selectionActionFactory;

    _tools.push_back(Tool({"Selection", FontAwesomeIcons::FA_MOUSE_POINTER, &selectionActionFactory, "Select by clicking in the 2D view"}));
    _tools.push_back(Tool({"Magnify", FontAwesomeIcons::FA_SEARCH, nullptr, "Click and hold and drag to zoom in/out"}));
    _tools.push_back(Tool({"Camera", FontAwesomeIcons::FA_CAMERA_RETRO, nullptr, "Move the camera"}));

    _tools.push_back(Tool({"Separator", nullptr, nullptr, ""}));

    _tools.push_back(Tool({"Entity tool", FontAwesomeIcons::FA_BULLSEYE, nullptr, "Create entities"}));
    _tools.push_back(Tool({"Block tool", FontAwesomeIcons::FA_CUBE, nullptr, "Create brushes"}));

    _tools.push_back(Tool({"Separator", nullptr, nullptr, ""}));

    _tools.push_back(Tool({"Texture application", FontAwesomeIcons::FA_IMAGE, nullptr, "Change texture on selected faces"}));
    _tools.push_back(Tool({"Apply current texture", FontAwesomeIcons::FA_FILE_IMAGE_O, nullptr, "Apply the current texture by click on faces"}));
    _tools.push_back(Tool({"Apply Decals", FontAwesomeIcons::FA_STEAM, nullptr, "Apply a decal by clicking"}));

    _tools.push_back(Tool({"Separator", nullptr, nullptr, ""}));

    _tools.push_back(Tool({"Clipping tool", FontAwesomeIcons::FA_SCISSORS, nullptr, "Clip brushes by plane"}));
    _tools.push_back(Tool({"Vertex tool", FontAwesomeIcons::FA_OBJECT_UNGROUP, nullptr, "Change the vertices of selected dfbrushes"}));
    _tools.push_back(Tool({"Path tool", FontAwesomeIcons::FA_CODE_FORK, nullptr, "Create path entities"}));
}

Tools::~Tools() {}

void Tools::selectTool(
    int index,
    struct app_state &state)
{
    this->_selectedTool = index;

    std::cout << "Selecting tool: " << index << std::endl;

    if (this->_selectedTool >= 0 && this->_selectedTool < this->_tools.size())
    {
        if (this->_tools[this->_selectedTool]._actionFactory != nullptr)
        {
            this->_tools[this->_selectedTool]._actionFactory->OnActivateTool(state);
        }
    }
}

int Tools::toolCount() const { return this->_tools.size(); }

const Tool &Tools::operator[](
    int index) const
{
    static Tool defaultTool;

    if (index >= 0 && index < this->_tools.size())
    {
        return this->_tools[index];
    }

    return defaultTool;
}

const Tool &Tools::selectedTool() const
{
    static Tool defaultTool;

    if (this->_selectedTool >= 0 && this->_selectedTool < this->_tools.size())
    {
        return this->_tools[this->_selectedTool];
    }

    return defaultTool;
}

int Tools::selectedToolIndex() const
{
    return this->_selectedTool;
}

bool Tools::isSelected(
    int index) const
{
    return this->_selectedTool == index;
}
