#ifndef TOOLS_H
#define TOOLS_H

#include <string>
#include <vector>

class Tool
{
public:
    std::string _name;
    const char *_icon;
    class BaseActionFactory *_actionFactory;
    std::string _tooltip;
};

class Tools
{
public:
    Tools();
    virtual ~Tools();

    void selectTool(
        int index,
        struct app_state &state);

    int toolCount() const;

    const Tool &selectedTool() const;

    int selectedToolIndex() const;

    bool isSelected(
        int index) const;

    const Tool &operator[](
        int index) const;

private:
    int _selectedTool;
    std::vector<Tool> _tools;
};

#endif // TOOLS_H
