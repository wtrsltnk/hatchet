#include "userinput.h"
#include <mutex>
#include <thread>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>

static std::mutex mappingsMutex;

bool operator < (
    UserInputEvent const &a,
    UserInputEvent const &b)
{
    if (a.source == b.source)
    {
        return a.key < b.key;
    }

    return a.source < b.source;
}

void UserInput::StartMappingAction(
    UserInputActions action)
{
    _mappingMode = true;
    _actionToMap = action;
}

std::vector<UserInputEvent> UserInput::GetMappedActionEvents(
    UserInputActions action)
{
    std::vector<UserInputEvent> result;

    for (auto pair : _stateMapping)
    {
        if (pair.second == action)
        {
            result.push_back(pair.first);
        }
    }

    return result;
}

void UserInput::ProcessEvent(
    UserInputEvent const &event,
    bool state)
{
    if (_mappingMode)
    {
        if (state)
        {
            // We only change mappings on the release of a button
            return;
        }

        std::lock_guard<std::mutex> lock(mappingsMutex);

        auto mapping = _stateMapping.find(event);
        if (mapping == _stateMapping.end())
        {
            _stateMapping.insert(std::make_pair(event, _actionToMap));
        }
        else
        {
            mapping->second = _actionToMap;
        }

        _mappingMode = false;
        return;
    }

    auto mapping = _stateMapping.find(event);
    if (mapping == _stateMapping.end())
    {
        return;
    }

    if (_actionStates.find(mapping->second) == _actionStates.end())
    {
        std::lock_guard<std::mutex> lock(mappingsMutex);

        _actionStates.insert(std::make_pair(mapping->second, false));
    }

    _actionStates[mapping->second] = state;
}

bool UserInput::ActionState(
    UserInputActions action)
{
    if (_actionStates.find(action) == _actionStates.end())
    {
        _actionStates.insert(std::make_pair(action, false));
    }

    return _actionStates[action];
}

void UserInput::ReadKeyMappings(
    std::filesystem::path const &filename)
{
    // we are threading this to make sure it will not freeze the menu or something
    std::thread t([this, filename]() {
        std::lock_guard<std::mutex> lock(mappingsMutex);

        std::ifstream infile(filename);

        if (!infile.is_open())
        {
            // TODO log this somewhere
            std::cerr << "could not open \"" << filename << "\" for reading" << std::endl;
            return;
        }

        _stateMapping.clear();

        std::string line;
        while (std::getline(infile, line))
        {
            std::istringstream iss(line);
            std::string action;
            unsigned int source;
            int key;
            iss >> std::quoted(action) >> source >> key;
            UserInputActions enumAction;

            for (int i = 0; i < int(UserInputActions::Count); ++i)
            {
                if (action == UserInputActionNames[i])
                {
                    enumAction = (UserInputActions)i;
                }
            }

            UserInputEvent uie = { source, key };
            _stateMapping.insert(std::make_pair(uie, enumAction));
        }

        infile.close();
    });

    t.detach();
}

void UserInput::WriteKeyMappings(
    std::filesystem::path const &filename)
{
    // we are threading this to make sure it will not freeze the menu or something
    std::thread t([this, filename]() {
        std::lock_guard<std::mutex> lock(mappingsMutex);

        std::ofstream outfile(filename);

        if (!outfile.is_open())
        {
            // TODO log this somewhere
            std::cerr << "could not open \"" << filename << "\" for writing" << std::endl;
            return;
        }

        for (auto pair : _stateMapping)
        {
            if (int(pair.second) < 0 || int(pair.second) >= int(UserInputActions::Count))
            {
                continue;
            }
            outfile << "\"" << UserInputActionNames[int(pair.second)] << "\" " << pair.first.source << " " << pair.first.key << std::endl;
        }

        outfile.close();
    });

    t.detach();
}

#include <GLFW/glfw3.h>

char const *UserInputEvent::toString()
{
    if (source == GLFW_PRESS)
    {
        switch (key)
        {
        case GLFW_KEY_ENTER: return "Enter";
        case GLFW_KEY_ESCAPE: return "Escape";
        case GLFW_KEY_BACKSPACE: return "Backspace";
        case GLFW_KEY_TAB: return "Tab";
        case GLFW_KEY_SPACE: return "SPACE";
        case GLFW_KEY_COMMA: return ",";
        case GLFW_KEY_MINUS: return "-";
        case GLFW_KEY_PERIOD: return ".";
        case GLFW_KEY_SLASH: return "/";
        case GLFW_KEY_0: return "0";
        case GLFW_KEY_1: return "1";
        case GLFW_KEY_2: return "2";
        case GLFW_KEY_3: return "3";
        case GLFW_KEY_4: return "4";
        case GLFW_KEY_5: return "5";
        case GLFW_KEY_6: return "6";
        case GLFW_KEY_7: return "7";
        case GLFW_KEY_8: return "8";
        case GLFW_KEY_9: return "9";
        case GLFW_KEY_SEMICOLON: return ";";
        case GLFW_KEY_LEFT_BRACKET: return "[";
        case GLFW_KEY_BACKSLASH: return "\\";
        case GLFW_KEY_RIGHT_BRACKET: return "]";
        case GLFW_KEY_GRAVE_ACCENT: return "`";
        case GLFW_KEY_A: return "a";
        case GLFW_KEY_B: return "b";
        case GLFW_KEY_C: return "c";
        case GLFW_KEY_D: return "d";
        case GLFW_KEY_E: return "e";
        case GLFW_KEY_F: return "f";
        case GLFW_KEY_G: return "g";
        case GLFW_KEY_H: return "h";
        case GLFW_KEY_I: return "i";
        case GLFW_KEY_J: return "j";
        case GLFW_KEY_K: return "k";
        case GLFW_KEY_L: return "l";
        case GLFW_KEY_M: return "m";
        case GLFW_KEY_N: return "n";
        case GLFW_KEY_O: return "o";
        case GLFW_KEY_P: return "p";
        case GLFW_KEY_Q: return "q";
        case GLFW_KEY_R: return "r";
        case GLFW_KEY_S: return "s";
        case GLFW_KEY_T: return "t";
        case GLFW_KEY_U: return "u";
        case GLFW_KEY_V: return "v";
        case GLFW_KEY_W: return "w";
        case GLFW_KEY_X: return "x";
        case GLFW_KEY_Y: return "y";
        case GLFW_KEY_Z: return "z";
        case GLFW_KEY_F1: return "F1";
        case GLFW_KEY_F2: return "F2";
        case GLFW_KEY_F3: return "F3";
        case GLFW_KEY_F4: return "F4";
        case GLFW_KEY_F5: return "F5";
        case GLFW_KEY_F6: return "F6";
        case GLFW_KEY_F7: return "F7";
        case GLFW_KEY_F8: return "F8";
        case GLFW_KEY_F9: return "F9";
        case GLFW_KEY_F10: return "F10";
        case GLFW_KEY_F11: return "F11";
        case GLFW_KEY_F12: return "F12";

        case GLFW_KEY_LEFT_CONTROL: return "Left CTRL";
        case GLFW_KEY_RIGHT_CONTROL: return "Right CTRL";
        case GLFW_KEY_LEFT_SHIFT: return "Left SHIFT";
        case GLFW_KEY_RIGHT_SHIFT: return "Right SHIFT";
        case GLFW_KEY_LEFT_ALT: return "Left ALT";
        case GLFW_KEY_RIGHT_ALT: return "Right ALT";

        case GLFW_KEY_LEFT: return "Left Arrow";
        case GLFW_KEY_RIGHT: return "Right Arrow";
        case GLFW_KEY_UP: return "Up Arrow";
        case GLFW_KEY_DOWN: return "Down Arrow";
        }
    }
    if (int(key) < sizeof(char))
    {
        static char tmp[2];
        tmp[0] = key;
        tmp[1] = 0;
        return tmp;
    }
    return "<unknown>";
}
