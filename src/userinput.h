#ifndef USERINPUT_H
#define USERINPUT_H

#include <filesystem>
#include <map>
#include <string>
#include <vector>

enum class UserInputActions
{
    PerspectiveCameraForward,
    PerspectiveCameraBackward,
    PerspectiveCameraLeft,
    PerspectiveCameraRight,

    Count
};

static const char *UserInputActionNames[] = {
    "Free Camera Forward",
    "Free Camera Backward",
    "Free Camera Left",
    "Free Camera Right",
};

struct UserInputEvent
{
    unsigned int source;
    int key;

    char const *toString();
};

class UserInput
{
    std::map<UserInputActions, bool> _actionStates;
    std::map<UserInputEvent, UserInputActions> _stateMapping;

public:
    bool _mappingMode;
    UserInputActions _actionToMap;

    void StartMappingAction(
        UserInputActions action);

    std::vector<UserInputEvent> GetMappedActionEvents(
        UserInputActions action);

    void ProcessEvent(
        UserInputEvent const &event, bool state);

    bool ActionState(
        UserInputActions action);

    void ReadKeyMappings(
        std::filesystem::path const &filename);

    void WriteKeyMappings(
        std::filesystem::path const &filename);
};

#endif // USERINPUT_H
