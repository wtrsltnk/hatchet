#include "vertexbuffer.h"
#include "shader.h"
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

VertexBuffer::VertexBuffer()
    : _shaderName("mapShader")
{}

bool VertexBuffer::Setup(
    std::string const &shaderName)
{
    if (_shader == 0 || _shaderName != shaderName)
    {
        _shaderName = shaderName;
        _shader = LoadShaderProgram(_shaderName);
    }

    if (_shader == 0)
    {
        std::cout << "Failed to load shaders" << std::endl;
        return false;
    }

    glUseProgram(_shader);

    _u_p = glGetUniformLocation(_shader, "u_p");
    _u_v = glGetUniformLocation(_shader, "u_v");
    _u_m = glGetUniformLocation(_shader, "u_m");
    _u_tex = glGetUniformLocation(_shader, "u_tex");
    _u_view_type = glGetUniformLocation(_shader, "u_view_type");
    _u_brush_color = glGetUniformLocation(_shader, "u_brush_color");

    glUseProgram(0);

    glGenVertexArrays(1, &_vao);
    glGenBuffers(1, &_vbo);

    Bind();

    auto vertexAttrib = glGetAttribLocation(_shader, "vertex");
    glVertexAttribPointer(GLuint(vertexAttrib), 3, GL_FLOAT, GL_FALSE, sizeof(MapVertex), 0);
    glEnableVertexAttribArray(GLuint(vertexAttrib));

    auto normalAttrib = glGetAttribLocation(_shader, "normal");
    glVertexAttribPointer(GLuint(normalAttrib), 3, GL_FLOAT, GL_FALSE, sizeof(MapVertex), reinterpret_cast<const GLvoid *>(sizeof(glm::vec3)));
    glEnableVertexAttribArray(GLuint(normalAttrib));

    auto uvAttrib = glGetAttribLocation(_shader, "uv");
    glVertexAttribPointer(GLuint(uvAttrib), 3, GL_FLOAT, GL_FALSE, sizeof(MapVertex), reinterpret_cast<const GLvoid *>(sizeof(glm::vec3) + sizeof(glm::vec3)));
    glEnableVertexAttribArray(GLuint(uvAttrib));

    Unbind();

    return true;
}

void VertexBuffer::UploadVertexData(
    const std::vector<MapVertex> &data)
{
    vertCount = data.size();

    Bind();

    glBufferData(GL_ARRAY_BUFFER, GLsizeiptr(data.size() * sizeof(MapVertex)), 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, GLsizeiptr(data.size() * sizeof(MapVertex)), reinterpret_cast<const GLvoid *>(&data[0]));

    Unbind();
}

void VertexBuffer::Bind()
{
    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
}

void VertexBuffer::BindForRendering(
    glm::mat4 const &projection,
    glm::mat4 const &view,
    glm::mat4 const &model,
    int viewType)
{
    glUseProgram(_shader);

    glUniformMatrix4fv(_u_p, 1, false, glm::value_ptr(projection));
    glUniformMatrix4fv(_u_v, 1, false, glm::value_ptr(view));
    glUniformMatrix4fv(_u_m, 1, false, glm::value_ptr(model));
    glUniform1i(_u_tex, 0);
    glUniform1i(_u_view_type, viewType);
    glUniform4f(_u_brush_color, 1.0f, 1.0f, 1.0f, 1.0f);

    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);

    Bind();
}

void VertexBuffer::SetBrushColor(
    glm::vec4 const &color)
{
    glUseProgram(_shader);

    glUniform4f(_u_brush_color, color.x, color.y, color.z, color.w);
}

void VertexBuffer::Unbind()
{
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glUseProgram(0);
}
