#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

#include "geometry/GeometryQueryService.h"

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <string>

class VertexBuffer
{
    std::string _shaderName;
    GLuint _vao = 0, _vbo = 0, _shader = 0;
    GLuint _u_p = 0, _u_v = 0, _u_m = 0, _u_tex = 0, _u_view_type = 0, _u_brush_color = 0;

public:
    VertexBuffer();
    size_t vertCount = 0;
    bool Setup(std::string const &shaderName);
    void UploadVertexData(
        const std::vector<MapVertex> &data);

    void Bind();
    void BindForRendering(glm::mat4 const &projection, glm::mat4 const &view, glm::mat4 const &model, int viewType);
    void SetBrushColor(glm::vec4 const &color);
    void Unbind();
};

#endif // VERTEXBUFFER_H
