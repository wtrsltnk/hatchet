
#include <glad/glad.h>

#include <GLFW/glfw3.h>

#define GLM_ENABLE_EXPERIMENTAL
#include "geometry/GeometryQueryService.h"
#include "geometry/mapscene.h"
#include "vertexbuffer.h"
#include <filesystem>
#include <fstream>
#include <geometry/mapparser.h>
#include <glm/gtx/matrix_decompose.hpp>
#include <iostream>
#include <orthocamera.h>

using namespace std;

void OpenGLMessageCallback(
    unsigned source,
    unsigned type,
    unsigned id,
    unsigned severity,
    int length,
    const char *message,
    const void *userParam)
{
    (void)userParam;

    switch (severity)
    {
        case GL_DEBUG_SEVERITY_HIGH:
            std::cout << "CRITICAL";
            break;
        case GL_DEBUG_SEVERITY_MEDIUM:
            std::cout << "ERROR";
            break;
        case GL_DEBUG_SEVERITY_LOW:
            std::cout << "WARNING";
            break;
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            std::cout << "DEBUG";
            break;
        default:
            std::cout << "UNKNOWN";
            break;
    }

    std::cout << "\n    source    : " << source
              << "\n    message   : " << message
              << "\n    type      : " << type
              << "\n    id        : " << id
              << "\n    length    : " << length
              << "\n";
}

GLFWwindow *window;
MapScene scene;
GeometryQueryService renderData;
VertexBuffer vertexBuffer;
VertexBuffer gridVertexBuffer;
OrthoCamera _camera(ViewTypes::Top);
size_t a;
int _width = 200.0f;
int _height = 300.0f;
int _mousex = 0;
int _mousey = 0;

void loadMap(std::string const &filename)
{
    auto file = std::filesystem::canonical(filename);

    if (!std::filesystem::exists(file))
    {
        std::cout << file.string() << " does not exist" << std::endl;
        return;
    }

    std::ifstream t(filename);
    std::string str((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

    MapParser parser(str);

    parser.LoadScene(scene);

    renderData.Collect(scene);

    vertexBuffer.UploadVertexData(renderData._verts);
}

bool appSetUp()
{
    vertexBuffer.Setup("mapShader");

    loadMap("views.map");

    std::vector<MapVertex> gridData;

    int max = 2048, min = -max;
    gridData.push_back(MapVertex(glm::vec3(min, 0, 0), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
    gridData.push_back(MapVertex(glm::vec3(max, 0, 0), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
    gridData.push_back(MapVertex(glm::vec3(0, min, 0), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
    gridData.push_back(MapVertex(glm::vec3(0, max, 0), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
    for (int y = min; y <= max; y++)
    {
        if (y % 64 != 0) continue;

        gridData.push_back(MapVertex(glm::vec3(min, y, 0), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
        gridData.push_back(MapVertex(glm::vec3(max, y, 0), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
    }

    for (int x = min; x <= max; x++)
    {
        if (x % 64 != 0) continue;

        gridData.push_back(MapVertex(glm::vec3(x, min, 0), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
        gridData.push_back(MapVertex(glm::vec3(x, max, 0), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0)));
    }
    a = gridData.size();

    for (int y = min; y <= max; y++)
    {
        if (y % 8 != 0) continue;

        gridData.push_back(MapVertex(glm::vec3(min, y, 0), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0)));
        gridData.push_back(MapVertex(glm::vec3(max, y, 0), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0)));
    }

    for (int x = min; x <= max; x++)
    {
        if (x % 8 != 0) continue;

        gridData.push_back(MapVertex(glm::vec3(x, min, 0), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0)));
        gridData.push_back(MapVertex(glm::vec3(x, max, 0), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0)));
    }

    gridVertexBuffer.Setup("mapShader");
    gridVertexBuffer.UploadVertexData(gridData);

    return true;
}

void appRender()
{
    glViewport(0, 0, _width, _height);
    _camera.SetViewRect(glm::vec4(0.0f, 0.0f, _width, _height));
    _camera.UpdateProjection();

    glClearColor(114 / 255.0f, 144 / 255.0f, 154 / 255.0f, 255 / 255.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    gridVertexBuffer.BindForRendering(_camera.ProjectionMatrix(), _camera.ViewMatrix(), glm::mat4(1.0f), 6);

    glm::vec3 scale, translation, skew;
    glm::vec4 perspective;
    glm::quat orientation;
    glm::decompose(_camera.ProjectionMatrix() * _camera.ViewMatrix(), scale, orientation, translation, skew, perspective);

    if (glm::length(scale) > 1.0f)
    {
        gridVertexBuffer.SetBrushColor(glm::vec4(0.4f, 0.4f, 0.4f, 0.7f));
        glDrawArrays(GL_LINES, 0, gridVertexBuffer.vertCount);
    }
    else
    {
        gridVertexBuffer.SetBrushColor(glm::vec4(0.4f, 0.4f, 0.4f, 0.5f));
        glDrawArrays(GL_LINES, 0, a);
    }

    gridVertexBuffer.SetBrushColor(glm::vec4(0.0f, 0.4f, 0.9f, 1.0f));
    glDrawArrays(GL_LINES, 0, 4);

    gridVertexBuffer.Unbind();
}

void appCleanUp()
{
}

void KeyActionCallback(
    GLFWwindow *window,
    int key,
    int scancode,
    int action,
    int mods)
{
}

void CursorPosCallback(
    GLFWwindow *window,
    double x,
    double y)
{
    _mousex = x;
    _mousey = y;
}

const float zoomMultiplier = 1.1f;

void ScrollCallback(
    GLFWwindow *window,
    double x,
    double y)
{
    auto origin = glm::vec3(
        _mousex,
        _height - _mousey,
        0.0f);

    std::cout << origin.x << ", " << origin.y << ", " << origin.z << ", " << std::endl;
    auto mouseInWorld = glm::unProject(
        origin,
        _camera.ViewMatrix(),
        _camera.ProjectionMatrix(),
        glm::vec4(0.0f, 0.0f, _camera.ViewRect().z, _camera.ViewRect().w));

    _camera.Zoom(y < 0 ? 1.0f / zoomMultiplier : zoomMultiplier, mouseInWorld);
}

void MouseButtonCallback(
    GLFWwindow *window,
    int button,
    int action,
    int mods)
{
}

void ResizeCallback(
    GLFWwindow *window,
    int width,
    int height)
{
    _width = width;
    _height = height;
}

int main(
    int argc,
    char *argv[])
{
    (void)argc;
    (void)argv;

    if (glfwInit() == GLFW_FALSE)
    {
        return -1;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(1024, 768, "Hatchet", NULL, NULL);
    if (window == 0)
    {
        glfwTerminate();
        return -1;
    }

    glfwSetKeyCallback(window, KeyActionCallback);
    glfwSetCursorPosCallback(window, CursorPosCallback);
    glfwSetScrollCallback(window, ScrollCallback);
    glfwSetMouseButtonCallback(window, MouseButtonCallback);

    glfwSetWindowSizeCallback(window, ResizeCallback);
    glfwMakeContextCurrent(window);

    gladLoadGL();

    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(OpenGLMessageCallback, nullptr);

    glDebugMessageControl(
        GL_DONT_CARE,
        GL_DONT_CARE,
        GL_DEBUG_SEVERITY_NOTIFICATION,
        0,
        nullptr,
        GL_FALSE);

    ResizeCallback(window, 1024, 768);

    if (appSetUp())
    {
        while (glfwWindowShouldClose(window) == 0)
        {
            glfwWaitEventsTimeout(1.0 / 60.0);

            glClear(GL_COLOR_BUFFER_BIT);

            appRender();

            glfwSwapBuffers(window);
        }
        appCleanUp();
    }

    glfwTerminate();

    return 0;
}
