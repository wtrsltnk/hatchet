#include <string>
#include "doctest.h"
#include "mapscene.h"
#include "mapentity.h"
#include "mapbrush.h"
#include "mapparser.h"
#include "maprenderdata.h"

static std::string mapContent = "{ \
    \"classname\" \"worldspawn\" \
    \"message\" \"DUST by Dave Johnston - for CounterStrike\" \
    \"skyname\" \"des\" \
    \"MaxRange\" \"8000\" \
    \"classname\" \"worldspawn\" \
    \"mapversion\" \"220\" \
    \"wad\" \"\\program files\\valve\\half-life\\valve\\halflife.wad;\\program files\\valve\\half-life\\cstrike\\cs_dust.wad\" \
    { \
        ( 256 -640 -192 ) ( 0 -640 -192 ) ( 0 0 0 ) SANDROAD [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1 \
        ( 256 0 -64 ) ( 0 0 -64 ) ( 0 -640 -256 ) SANDROAD [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1 \
        ( 0 -640 -256 ) ( 0 0 -64 ) ( 0 0 0 ) SANDROAD [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1 \
        ( 256 -640 -192 ) ( 256 0 0 ) ( 256 0 -64 ) SANDROAD [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1 \
        ( 0 0 -64 ) ( 256 0 -64 ) ( 256 0 0 ) SANDROAD [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1 \
        ( 0 -640 -192 ) ( 256 -640 -192 ) ( 256 -640 -256 ) SANDROAD [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1 \
    } \
}";

TEST_CASE("Map entity is loaded")
{
    MapParser parser(mapContent);

    auto scene = new MapScene();
    REQUIRE(parser.LoadScene(scene) == true);
    REQUIRE(scene->_entities.size() == 1);

    auto entity = scene->_entities.front();
    REQUIRE(entity->_keyValuePairs.size() == 7);
    REQUIRE(entity->_brushes.size() == 1);

    auto brush = entity->_brushes.front();
    REQUIRE(brush->_sides.size() == 6);
}

TEST_CASE("No map entity loaded from empty string")
{
    MapParser parser("");

    auto scene = new MapScene();
    REQUIRE(parser.LoadScene(scene) == true);
}

TEST_CASE("One empty map entity loaded from empty brackets")
{
    MapParser parser("{ }");

    auto scene = new MapScene();
    REQUIRE(parser.LoadScene(scene) == true);
    REQUIRE(scene->_entities.size() == 1);
}

TEST_CASE("Map entity into map render data")
{
    MapParser parser(mapContent);
    MapRenderData renderData;

    auto scene = new MapScene();
    REQUIRE(parser.LoadScene(scene) == true);

    renderData.Collect(scene);

    REQUIRE(renderData._data.size() == 1);
    REQUIRE(renderData._data.begin()->first == std::string("SANDROAD"));
    REQUIRE(renderData._data.begin()->second.size() == 6);
    REQUIRE(renderData._data.begin()->second.begin()->_firstVertex == 0);
    REQUIRE(renderData._data.begin()->second.begin()->_vertexCount == 4);
}
