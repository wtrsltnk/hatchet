#include <string>
#include "doctest.h"
#include "tokenizer.h"

TEST_CASE("Tokenizer reading two tokens")
{
    std::string subject = "two words";

    Tokenizer tok(subject);

    REQUIRE(tok.get() == "");
    REQUIRE(tok.next() == true);
    REQUIRE(tok.get() == "two");
    REQUIRE(tok.next() == true);
    REQUIRE(tok.get() == "words");
    REQUIRE(tok.next() == false);
}

TEST_CASE("Tokenizer reading tokens in quotes")
{
    std::string subject = "\"two words\"";

    Tokenizer tok(subject);

    REQUIRE(tok.get() == "");
    REQUIRE(tok.next() == true);
    REQUIRE(tok.get() == "two words");
    REQUIRE(tok.next() == false);
}
